Dependencies to be installed to successfully compile Remote Media Control
=========================================================================

Before you can compile the solution, please install all the mandatory compoenents below:

Mandatory:

Windows Identity Foundation Runtime (MSU file):             http://www.microsoft.com/download/en/details.aspx?id=17331
Windows Identity Foundation SDK 4.0:                        http://www.microsoft.com/download/en/details.aspx?id=4451
Windows Azure Tools for Visual Studio 2010:                 http://www.microsoft.com/web/gallery/install.aspx?appid=WindowsAzureToolsVS2010

Optional:

NUnit (only for the test runner if you don't want to use    http://nunit.org/?p=download
Visual Studio):