﻿using System;
using Castle.Facilities.WcfIntegration;
using Castle.Windsor;
using CloudMedia.Remote.Framework.ServiceLocation;

namespace CloudMedia.Remote.Desktop.Configuration
{
    public interface IContainerBuilder : IDisposable
    {
        /// <summary>
        /// Build all dependencies.
        /// </summary>
        IServiceLocator Build();

        DefaultServiceHostFactory ServiceHostFactory { get; }

        /// <summary>
        /// Gets the castle windsor container reference.
        /// </summary>
        IWindsorContainer Container { get; }
    }
}
