﻿using System;
using Castle.Core;
using Castle.DynamicProxy;
using Castle.Facilities.Logging;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CloudMedia.Remote.Desktop.MediaCentre.Commands;
using CloudMedia.Remote.Desktop.Services;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Framework.Instrumentation;
using CloudMedia.Remote.Framework.ServiceLocation;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.Configuration
{
    public class DefaultContainerBuilder : IContainerBuilder
    {
        private readonly IWindsorContainer _container;
        private readonly DefaultServiceHostFactory _serviceHostFactory;
        private WindsorServiceLocator _windsorServiceLocator;
        private bool _disposed;

        public DefaultContainerBuilder()
        {
            _container = new WindsorContainer();
            _container.AddFacility<WcfFacility>();
            // doc : http://stw.castleproject.org/(S(su2mgm45fclhqe55veo40545))/Windsor.Logging-Facility.ashx

            // TO DO fix log 4 net - not working...
            //_container.AddFacility<LoggingFacility>(f => f.LogUsing(LoggerImplementation.Log4net).WithConfig("log4net.config")); 
            _serviceHostFactory = new DefaultServiceHostFactory(_container.Kernel);
        }

        /// <summary>
        /// Gets an instance of the castle container.
        /// </summary>
        public IWindsorContainer Container
        {
            get { return _container; }
        }

        public DefaultServiceHostFactory ServiceHostFactory
        {
            get { return _serviceHostFactory; }
        }

        public IServiceLocator Build()
        {
            BuildInterceptors();
            BuildServices();
            BuildAndRegisterServiceLocator();
            BuildCommands();

            return _windsorServiceLocator;
        }

        protected virtual void BuildServices()
        {
            Container.Register(Component.For<ITelevisionService>().ImplementedBy<TelevisionService>()
                                   .Interceptors(InterceptorReference.ForKey("GlobalInterceptor")).Anywhere);
            Container.Register(Component.For<ITelevisionGuideService>().ImplementedBy<TelevisionGuideService>()
                                   .Interceptors(InterceptorReference.ForKey("GlobalInterceptor")).Anywhere);
            Container.Register(Component.For<IStatisticsService>().ImplementedBy<StatisticsService>()
                             .Interceptors(InterceptorReference.ForKey("GlobalInterceptor")).Anywhere);
            Container.Register(Component.For<IDynDnsService>().ImplementedBy<DynDnsService>()
                             .Interceptors(InterceptorReference.ForKey("GlobalInterceptor")).Anywhere);

        }

        private void BuildAndRegisterServiceLocator()
        {
            _windsorServiceLocator = new WindsorServiceLocator(_container);
            ServiceLocator.SetLocatorProvider(() => _windsorServiceLocator);

            // now register the service locator with castle..
            _container.Register(Component.For<IServiceLocator>().Instance(_windsorServiceLocator));
        }

        public void BuildInterceptors()
        {
            Container.Register(Component.For<IInterceptor>()
                                        .ImplementedBy<ServiceBusInstrumentationInterceptor>()
                                        .Named("GlobalInterceptor"));
            
        }
     
        public void BuildCommands()
        {
            Container.Register(Component.For<ICommandAbstractFactory>()
                                   .ImplementedBy<CommandAbstractFactory>()
                                   .LifestyleSingleton());

            Container.Register(Component.For<ICommand<RetrieveMediaCentreStatisticsContext>>()
                                   .ImplementedBy<RetrieveMediaCentreStatisticsCommand>()
                                   .LifestyleTransient());

            Container.Register(Component.For<ICommand<RetrieveMediaCentreEpgContext>>()
                           .ImplementedBy<RetrieveMediaCentreEpgCommand>()
                           .LifestyleTransient());

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _container.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
