﻿using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.ServiceBus;
using CloudMedia.Remote.Desktop.MediaCentre.Commands;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.Client.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Press [Enter] when the server is ready");
            System.Console.ReadLine();
            var factory = new ChannelFactory<IStatisticsService>("CloudMedia.Remote.Desktop.Services.StatisticsService");
            var client = factory.CreateChannel();
            var clientChannel = (IChannel)client;
            clientChannel.Open();
            var status = clientChannel.GetProperty<IHybridConnectionStatus>();
            bool upgraded = false;
            if (status != null)
            {
                status.ConnectionStateChanged +=
                  (o, e) =>
                  {
                      System.Console.WriteLine("Upgraded to hybrid");
                      upgraded = true;
                      System.Console.WriteLine("Press any key to continue");
                      System.Console.ReadLine();
                  };
            }

            int total = 1000;
            for (var i = 0; i < total; ++i)
            {
                var sw = Stopwatch.StartNew();
                var response = client.RetrieveStatistics();
                sw.Stop();
                System.Console.WriteLine(response.SystemDate.ToShortDateString() + " " + response.SystemDate.ToLongTimeString() + " " + i.ToString() + " of " + total + " took (ms): " +  sw.Elapsed, factory.Endpoint.ListenUri);
            }
            clientChannel.Close();
            System.Console.ReadLine();

            //ServiceBusEnvironment.SystemConnectivity.Mode = ConnectivityMode.AutoDetect;
            //string serviceNamespace = "simonrhart"; //System.Console.ReadLine();
         
            //System.Console.WriteLine("Your Service Namespace: " + serviceNamespace);
            //string issuerName = "owner"; //= System.Console.ReadLine();
        
            //System.Console.WriteLine("Your Issuer Name: " + issuerName);
            //string issuerSecret = "l4A506TRSmYZdBPMPgTaIdMrAxubCgNQ72jYFlhJaFc="; //System.Console.ReadLine();

            //System.Console.WriteLine("Your Issuer Secret: " + issuerSecret);
         
            //Uri serviceUri = ServiceBusEnvironment.CreateServiceUri("sb", serviceNamespace, "TelevisionService");

            //TransportClientEndpointBehavior sharedSecretServiceBusCredential = new TransportClientEndpointBehavior();
            //sharedSecretServiceBusCredential.TokenProvider = TokenProvider.CreateSharedSecretTokenProvider(issuerName, issuerSecret);


            //ChannelFactory<ITelevisionServiceChannel> channelFactory = new ChannelFactory<ITelevisionServiceChannel>("RelayEndpoint", new EndpointAddress(serviceUri));

            //channelFactory.Endpoint.Behaviors.Add(sharedSecretServiceBusCredential);

       
            //System.Console.WriteLine("Enter text to echo (or [Enter] to exit):");
            //string input = System.Console.ReadLine();
            //while (input != String.Empty)
            //{

            //    ITelevisionServiceChannel channel = channelFactory.CreateChannel();
            //    channel.Open();

            //    try
            //    {
            //        System.Console.WriteLine("Server echoed: {0}", channel.Echo(input));
            //    }
            //    catch (Exception e)
            //    {
            //        System.Console.WriteLine("Error: " + e.Message);
            //    }
            //    input = System.Console.ReadLine();

            //    channel.Close();

            //}

            //channelFactory.Close();


        }
    }
}
