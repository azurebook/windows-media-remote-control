﻿using NUnit.Framework;
using CloudMedia.Remote.Desktop.MediaCentre.Commands;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Framework.ServiceLocation;

namespace CloudMedia.Remote.Desktop.Configuration.Tests
{
    public class DefaultContainerBuilderTests
    {
        private IContainerBuilder builder;
        private IServiceLocator locator;

        [TestFixtureSetUp]
        public void Setup()
        {
            builder = new DefaultContainerBuilder();
            locator = builder.Build();
        }

        [Test]
        public void CanGetServiceLocator()
        {
            var serviceLocator = locator.GetInstance<IServiceLocator>();
            var serviceLocator2 = locator.GetInstance<IServiceLocator>();

            Assert.AreSame(serviceLocator, serviceLocator2);
        }

        [Test]
        public void CanGetCommands()
        {
            Assert.IsNotNull(locator.GetInstance<ICommandAbstractFactory>());

            Assert.IsNotNull(locator.GetInstance<ICommand<RetrieveMediaCentreStatisticsContext>>());

            Assert.IsNotNull(locator.GetInstance<ICommand<RetrieveMediaCentreEpgContext>>());
    
        }
    }
}
