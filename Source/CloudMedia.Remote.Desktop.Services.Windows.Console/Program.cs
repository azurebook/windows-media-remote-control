﻿using System;
using System.ServiceModel.Description;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Description;
using CloudMedia.Remote.Desktop.Configuration;
using CloudMedia.Remote.Services.Contracts;
using Microsoft.ServiceBus.Messaging;

namespace CloudMedia.Remote.Desktop.Services.Windows.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            IContainerBuilder builder = new DefaultContainerBuilder();
            builder.Build();

            var televisionService = builder.ServiceHostFactory.CreateServiceHost(typeof(ITelevisionService).AssemblyQualifiedName, new Uri[0]);
                        
            foreach (ServiceEndpoint endpoint in televisionService.Description.Endpoints)
            {
                System.Console.WriteLine("Name: {0}: URI: {1}", endpoint.Contract.Name, endpoint.ListenUri);
            }

            televisionService.Open();

            var statisticsService = builder.ServiceHostFactory.CreateServiceHost(typeof(IStatisticsService).AssemblyQualifiedName, new Uri[0]);

            foreach (ServiceEndpoint endpoint in statisticsService.Description.Endpoints)
            {
                System.Console.WriteLine("Name: {0}: URI: {1}", endpoint.Contract.Name, endpoint.ListenUri);
            }

            statisticsService.Open();

            var televisionGuideService = builder.ServiceHostFactory.CreateServiceHost(typeof(ITelevisionGuideService).AssemblyQualifiedName, new Uri[0]);

            foreach (ServiceEndpoint endpoint in televisionGuideService.Description.Endpoints)
            {
                System.Console.WriteLine("Name: {0}: URI: {1}", endpoint.Contract.Name, endpoint.ListenUri);
            }

            televisionGuideService.Open();
            
            
            System.Console.WriteLine("Press [Enter] to exit");
            System.Console.ReadLine();

            televisionService.Close();
            televisionGuideService.Close();
            statisticsService.Close();

            builder.Dispose();
        }

        private static void CreateTopic()
        {
            System.Console.WriteLine("Check if topic 'record' exists...");
            TokenProvider tokenProvider = TokenProvider.CreateSharedSecretTokenProvider("owner", "[YOUR_SECRET]");
            Uri serviceUri = ServiceBusEnvironment.CreateServiceUri("sb", "[YOUR_NAMESPACE]", string.Empty);


            var namespaceManager = new NamespaceManager(serviceUri, tokenProvider);

            if (!namespaceManager.TopicExists("record"))
            {
                System.Console.WriteLine("Topic 'record' does not exist");
                var topicDescription = new TopicDescription("record");
                topicDescription.MaxSizeInMegabytes = 5120;
                topicDescription.DefaultMessageTimeToLive = TimeSpan.MaxValue;

                namespaceManager.CreateTopic(topicDescription);
                System.Console.WriteLine("Created topic 'Record'");

                System.Console.WriteLine("Creating subscription 'All'");
                SubscriptionDescription subscriptionDescription = new SubscriptionDescription(topicDescription.Path, "All");
                subscriptionDescription.DefaultMessageTimeToLive = TimeSpan.MaxValue;
                subscriptionDescription.EnableDeadLetteringOnMessageExpiration = true;
                subscriptionDescription.EnableDeadLetteringOnFilterEvaluationExceptions = true;
                namespaceManager.CreateSubscription(subscriptionDescription);

                System.Console.WriteLine("Created subscription 'All'");
            }
        }
    }
}
