﻿using Moq;
using NUnit.Framework;
using CloudMedia.Remote.Desktop.MediaCentre.Commands;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.Services.Tests
{
    public class StatisticsServiceTests
    {
        private StatisticsService statisticsService;
        private Mock<ICommandAbstractFactory> _mockCommandAbstractFactory;
             
        [TestFixtureSetUp]
        public void Setup()
        {
            _mockCommandAbstractFactory = new Mock<ICommandAbstractFactory>(MockBehavior.Strict);
            statisticsService = new StatisticsService(_mockCommandAbstractFactory.Object);
        }

        /// <summary>
        /// Ensure we call the correct command behind the service.
        /// </summary>
        [Test]
        public void CanGetStatistics()
        {
            // Arrange
            _mockCommandAbstractFactory.Setup(x => x.ExecuteCommand(It.IsAny<RetrieveMediaCentreStatisticsContext>()));

            // Act
            statisticsService.RetrieveStatistics();

            // Assert
            _mockCommandAbstractFactory.Verify(x => x.ExecuteCommand(It.IsAny<RetrieveMediaCentreStatisticsContext>()), Times.Once());
        }
    }
}
