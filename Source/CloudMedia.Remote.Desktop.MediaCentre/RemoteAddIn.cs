﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.MediaCenter.Hosting;

namespace CloudMedia.Remote.Desktop.MediaCentre
{
    public class RemoteAddIn : IRemoteAddIn
    {
        private AddInHost _host;

        public void Launch(AddInHost host)
        {
            _host = host;
            host.MediaCenterEnvironment.Dialog("Add-in launched successfully", "Hello World!", Microsoft.MediaCenter.DialogButtons.Ok, 100, false);
	
 		
        }

        public AddInHost Host
        {
            get { return _host; }
        }

        public void Initialize(Dictionary<string, object> appInfo, Dictionary<string, object> entryPointInfo)
        {
        }

        public void Uninitialize()
        {
        }
    }
}
