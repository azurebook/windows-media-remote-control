﻿using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.MediaCentre.Commands
{
    public class RetrieveMediaCentreStatisticsContext
    {
        public MediaCentreStatisticsResponse Response { get; set; }
    }
}
