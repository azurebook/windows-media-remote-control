﻿using System;
using System.IO;
using System.Net;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.MediaCentre.Commands
{
    public class RetrieveMediaCentreStatisticsCommand : BaseCommand<RetrieveMediaCentreStatisticsContext>
    {
        private readonly IDynDnsService _dynDnsService;

        public RetrieveMediaCentreStatisticsCommand(IDynDnsService dynDnsService)
        {
            _dynDnsService = dynDnsService;
        }

        /// <summary>
        /// Executes this command the place the result on the passed context as a response.
        /// </summary>
        /// <param name="context"></param>
        protected override void DoExecute(RetrieveMediaCentreStatisticsContext context)
        {
            context.Response = new MediaCentreStatisticsResponse();
            context.Response.MachineName = Environment.MachineName;
            context.Response.OperatingSystemVersion = Environment.Is64BitOperatingSystem
                                                          ? Environment.OSVersion.VersionString + " " + "x64"
                                                          : Environment.OSVersion.VersionString + " " + "x86";
            context.Response.Uptime = Environment.TickCount;
            context.Response.LoggedOnUser = Environment.UserName;
            context.Response.DomainName = Environment.UserDomainName;
            context.Response.ProcessorCount = Environment.ProcessorCount;
            context.Response.SystemPageSize = Environment.SystemPageSize;
            context.Response.SystemDate = DateTime.Now;
            context.Response.ExternalIP = _dynDnsService.RetrieveExternalIp().ExternalIp;
        }

        protected override ExceptionAction HandleError(RetrieveMediaCentreStatisticsContext context, Exception exception)
        {
            return ExceptionAction.Rethrow;
        }
    }
}
