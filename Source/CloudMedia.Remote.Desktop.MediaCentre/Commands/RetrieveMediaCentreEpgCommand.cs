﻿using System;
using Microsoft.MediaCenter.TV.Epg;
using CloudMedia.Remote.Framework.Command;

namespace CloudMedia.Remote.Desktop.MediaCentre.Commands
{
    public class RetrieveMediaCentreEpgCommand : BaseCommand<RetrieveMediaCentreEpgContext>
    {
        protected override void DoExecute(RetrieveMediaCentreEpgContext context)
        {
            Lineup lineup = new Lineup();
            var response = lineup.GetCallSigns();

        }

        protected override ExceptionAction HandleError(RetrieveMediaCentreEpgContext context, Exception exception)
        {
            return ExceptionAction.Rethrow;
        }
    }
}
