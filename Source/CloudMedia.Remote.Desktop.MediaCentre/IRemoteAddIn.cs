﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.MediaCenter.Hosting;

namespace CloudMedia.Remote.Desktop.MediaCentre
{
    public interface IRemoteAddIn : IAddInModule, IAddInEntryPoint
    {
        AddInHost Host { get; }
    }
}
