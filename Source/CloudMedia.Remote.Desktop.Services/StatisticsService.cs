﻿using System.ServiceModel;
using CloudMedia.Remote.Desktop.MediaCentre.Commands;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.Services
{
    [ServiceBehavior(Name = "StatisticsService", Namespace = "http://remotemedia.simonrhar.com/2011/12")]
    public class StatisticsService : IStatisticsService
    {
        private readonly ICommandAbstractFactory _commandAbstractFactory;

        public StatisticsService(ICommandAbstractFactory commandAbstractFactory)
        {
            _commandAbstractFactory = commandAbstractFactory;
        }

        public MediaCentreStatisticsResponse RetrieveStatistics()
        {
            var context = new RetrieveMediaCentreStatisticsContext();
            _commandAbstractFactory.ExecuteCommand(context);
            return context.Response;
        }
    }
}
