﻿using System;
using CloudMedia.Remote.Services.Contracts;
using System.ServiceModel;

namespace CloudMedia.Remote.Desktop.Services
{
    [ServiceBehavior(Name = "TelevisionService", Namespace = "http://remotemedia.simonrhart.com/2011/12")]
    public class TelevisionGuideService : ITelevisionGuideService
    {
        public string Echo(string text)
        {
            Console.WriteLine("Received message text: {0}", text);
            return text;
        }
    }
}
