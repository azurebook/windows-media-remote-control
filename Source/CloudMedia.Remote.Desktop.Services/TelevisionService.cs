﻿using System;
using System.ServiceModel;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.Services
{
    [ServiceBehavior(Name = "TelevisionService", Namespace = "http://remotemedia.simonrhart.com/2011/12")]
    public class TelevisionService : ITelevisionService
    {       
        public void Record(string text)
        {
            Console.WriteLine("Received message text: {0}", text);
        }
    }
}
