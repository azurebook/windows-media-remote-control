﻿using System.IO;
using System.Net;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Desktop.Services
{
    public class DynDnsService : IDynDnsService
    {
        public ExternalIpResponse RetrieveExternalIp()
        {
            var client = new WebClient();

            // Add a user agent header in case the requested URI contains a query.
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR1.0.3705;)");

            Stream data = client.OpenRead("http://checkip.dyndns.org/");

            var reader = new StreamReader(data);

            string response = reader.ReadToEnd();

            data.Close();

            reader.Close();

            ExternalIpResponse externalIpResponse = new ExternalIpResponse();
            externalIpResponse.ExternalIp = response.Replace("<html><head><title>Current IP Check</title></head><body>Current IP Address: ", "")
                           .Replace("</body></html>", "");

            return externalIpResponse;
        }
    }
}
