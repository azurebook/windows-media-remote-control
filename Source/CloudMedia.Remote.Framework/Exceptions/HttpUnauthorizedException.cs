﻿using System;

namespace CloudMedia.Remote.Framework.Exceptions
{
    public class HttpUnauthorizedException : Exception
    {
        public HttpUnauthorizedException()
        {
        }

        public HttpUnauthorizedException(string message):base(message)
        {
        }

        public HttpUnauthorizedException(string message, Exception inner):base(message, inner)
        {
        }
    }
}
