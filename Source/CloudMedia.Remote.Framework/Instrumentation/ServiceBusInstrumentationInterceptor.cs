﻿using System;
using System.Diagnostics;
using Castle.DynamicProxy;

namespace CloudMedia.Remote.Framework.Instrumentation
{
    public class ServiceBusInstrumentationInterceptor : IInterceptor
    {
        /// <summary>
        /// Intercepts the specified invocation.
        /// </summary>
        /// <param name="invocation">The invocation.</param>
        public void Intercept(IInvocation invocation)
        {
            var sw = Stopwatch.StartNew();

            Console.WriteLine(string.Format("Service operation called: {0}", invocation.Method.Name));
            invocation.Proceed();
            Console.WriteLine(string.Format("Service operation completed in: {0}", sw.Elapsed));

            sw.Stop();
        }
    }
}
