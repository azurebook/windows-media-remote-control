﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CloudMedia.Remote.Framework.Command
{
    /// <summary>
    /// Interface for command.
    /// </summary>
    /// <typeparam name="TContext">The context type.</typeparam>
    public interface ICommand<TContext>
    {
        /// <summary>
        /// Executes the context.
        /// </summary>
        /// <param name="context">The context to execute.</param>
        void Execute(TContext context);
    }
}
