﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CloudMedia.Remote.Framework.Tests.Command
{
    /// <summary>
    /// Mock context to test abstract factory.
    /// </summary>
    public class MockContext
    {
        /// <summary>
        /// Gets or sets the action used for testing for execution.
        /// </summary>
        public Action ExecuteAction { get; set; }

        /// <summary>
        /// Gets or sets the action used for testing for handling errors.
        /// </summary>
        public Action ErrorAction { get; set; }

        /// <summary>
        /// Gets or sets the action used for testing dispose.
        /// </summary>
        public Action DisposeAction { get; set; }
    }
}



