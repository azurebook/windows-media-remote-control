﻿// <copyright company="Aviva" file="CommandAbstractFactoryTests.cs">
// Copyright © 2011, Aviva
// application="MyPolicy 2.0"
// </copyright>

using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CloudMedia.Remote.Framework.Command;
using System;
using NUnit.Framework;
using CloudMedia.Remote.Framework.ServiceLocation;

namespace CloudMedia.Remote.Framework.Tests.Command
{
    /// <summary>
    /// Tests for the command abstract factory. Decided to use manual mocks instead of Moq for 
    /// ease of readability.
    /// </summary>
    [TestFixture]
    public class CommandAbstractFactoryTests
    {
        /// <summary>
        /// Our abstract factory instance.
        /// </summary>
        private ICommandAbstractFactory factory;

        /// <summary>
        /// The context to test against.
        /// </summary>
        private MockContext context;

        /// <summary>
        /// Sets up this instance.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<ICommand<MockContext>>().ImplementedBy<MockCommand>());
            var resolver = new WindsorServiceLocator(container);
            this.factory = new CommandAbstractFactory(resolver);
            this.context = new MockContext();
            this.context.DisposeAction = () => Console.WriteLine(string.Format("Cleaning up {0}", this.context.GetType()));
        }

        /// <summary>
        /// Checks whether we can execute command.
        /// </summary>
        [Test]
        public void CanExecuteSuccessfulCommand()
        {
            // Arrange
            var hasExecuted = false;

            this.context.ExecuteAction = () => hasExecuted = true;
            this.context.ErrorAction = () => Assert.Fail("Should not error");

            // Act
            this.factory.ExecuteCommand(this.context);

            // Assert
            Assert.IsTrue(hasExecuted);
        }

        /// <summary>
        /// Checks whether we handle an error in the command or not.
        /// </summary>
        [Test]
        public void CanHandleErrorInCommand()
        {
            // Arrange
            var exception = new Exception("Generic exception");
            var hasExecuted = false;
            this.context.ErrorAction = () => hasExecuted = true;
            this.context.ExecuteAction = () => { throw exception; };

            // Act
            this.factory.ExecuteCommand(this.context);

            // Assert
            Assert.IsTrue(hasExecuted);
        }

        /// <summary>
        /// Checks that the factory calls dispose on cleanup.
        /// </summary>
        [Test]
        public void ShouldDisposeCommand()
        {
            // Arrange
            var hasDisposed = false;
            this.context.ErrorAction = () => Assert.Fail("Should not error");
            this.context.ExecuteAction = () => Console.WriteLine("Executing command...");
            this.context.DisposeAction = () => hasDisposed = true;

            // Act
            this.factory.ExecuteCommand(this.context);

            // Assert
            Assert.IsTrue(hasDisposed);
        }
    }
}
