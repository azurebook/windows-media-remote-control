﻿using System;
using CloudMedia.Remote.Framework.Command;

namespace CloudMedia.Remote.Framework.Tests.Command
{ 
    /// <summary>
    /// Mock command used to test abstract factory.
    /// </summary>
    public class MockCommand : BaseCommand<MockContext>, IDisposable
    {
        /// <summary>
        /// Our mocked context.
        /// </summary>
        private MockContext context;

        /// <summary>
        /// Clean up.
        /// </summary>
        public void Dispose()
        {
            this.context.DisposeAction();
        }

        /// <summary>
        /// Executes the action.
        /// </summary>
        /// <param name="context">Context to execute.</param>
        protected override void DoExecute(MockContext context)
        {
            this.context = context;
            context.ExecuteAction();
        }

        /// <summary>
        /// Handle error.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="exception">The exception.</param>
        /// <returns>Action as what to do.</returns>
        protected override ExceptionAction HandleError(MockContext context, Exception exception)
        {
            context.ErrorAction();
            return ExceptionAction.Suppress;
        }
    }
}

