﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using CloudMedia.Remote.Cloud.Web.UI.Configuration;

namespace CloudMedia.Remote.Cloud.Web.UI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private IContainerBuilder _containerBuilder;

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute(
               "CDN",
               "cdn", 
             new { controller = "Guide", action = "Index", id = UrlParameter.Optional }); // Parameter defaults
     

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

         
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            _containerBuilder = new DefaultContainerBuilder();
            _containerBuilder.Build();
        }

        protected void Application_End()
        {
            _containerBuilder.Dispose();
        }
    }
}