﻿using System.Web.Mvc;

namespace CloudMedia.Remote.Cloud.Web.UI.Models
{
    public class HomeModel
    {
        public MvcHtmlString WelcomeMessage { get; set; }
    }
}