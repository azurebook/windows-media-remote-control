﻿using System;

namespace CloudMedia.Remote.Cloud.Web.UI.Models
{
    public class StatisticsModel
    {
        public string MachineName { get; set; }

        public string OperatingSystemVersion { get; set; }

        public string Uptime { get; set; }

        public string LoggedOnUser { get; set; }

        public string SystemPageSize { get; set; }

        public int ProcessorCount { get; set; }

        public DateTime SystemDate { get; set; }

        public string ExternalIP { get; set; }
    }
}