﻿namespace CloudMedia.Remote.Cloud.Web.UI.Models
{
    public class AuthorizationStatusModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether the user is a reader.
        /// </summary>
        public bool IsReader { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is a administrator.
        /// </summary>
        public bool IsAdministrator { get; set; }

        public bool IsAuthenticated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the log on users full name - comes from the identity provider mapped from the STS (ACS in Azure).
        /// </summary>
        public string FullName { get; set; }
    }
}