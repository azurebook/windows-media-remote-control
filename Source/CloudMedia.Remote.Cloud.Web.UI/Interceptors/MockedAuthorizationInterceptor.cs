﻿using Castle.DynamicProxy;
using CloudMedia.Remote.Cloud.Web.UI.Controllers;
using CloudMedia.Remote.Cloud.Web.UI.Models;

namespace CloudMedia.Remote.Cloud.Web.UI.Interceptors
{
    public class MockedAuthorizationInterceptor : IInterceptor
    {
        /// <summary>
        /// Intercepts the specified invocation.
        /// </summary>
        /// <param name="invocation">The invocation.</param>
        public void Intercept(IInvocation invocation)
        {
            var controller = invocation.Proxy as SuperController;
            if (controller != null)
            {
                var model = new AuthorizationStatusModel();
                if (controller.User != null)
                {
                    model.IsAdministrator = true;
                    model.IsReader = true;
                    model.IsAuthenticated = true;
                    model.FullName = "Mocked User";
                }
                controller.ViewBag.Authorization = model;

            }

            // forward the call onto the controller.
            invocation.Proceed();
        }
    }
}