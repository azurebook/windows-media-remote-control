﻿using System.Web.Mvc;
using Castle.DynamicProxy;
using Microsoft.IdentityModel.Claims;
using CloudMedia.Remote.Cloud.Web.UI.Controllers;
using CloudMedia.Remote.Cloud.Web.UI.Models;

namespace CloudMedia.Remote.Cloud.Web.UI.Interceptors
{
    public class AuthorizationInterceptor : IInterceptor
    {
        /// <summary>
        /// Intercepts the specified invocation.
        /// </summary>
        /// <param name="invocation">The invocation.</param>
        public void Intercept(IInvocation invocation)
        {
            var controller = invocation.Proxy as SuperController; 
            if (controller != null)
            {
                var model = new AuthorizationStatusModel();
                model.FullName = "Stranger!";
                if (controller.User != null)
                {
                    model.IsAdministrator = controller.User.IsInRole("Admin");
                    model.IsReader = controller.User.IsInRole("Reader");
                    model.IsAuthenticated = controller.User.Identity.IsAuthenticated;
                    if (model.IsAuthenticated)
                    {
                        model.FullName = controller.User.Identity.Name;
                    }
                }
                controller.ViewBag.Authorization = model;
          
            }

            // forward the call onto the controller.
            invocation.Proceed();
        }
    }
}