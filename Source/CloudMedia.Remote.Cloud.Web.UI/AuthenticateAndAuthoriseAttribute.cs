﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CloudMedia.Remote.Framework.Exceptions;
using Microsoft.IdentityModel.Protocols.WSFederation;
using Microsoft.IdentityModel.Web;

namespace CloudMedia.Remote.Cloud.Web.UI
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class AuthenticateAndAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        public string Roles { get; set; }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                AuthenticateUser(filterContext);
            else
                AuthorizeUser(filterContext);
        }

        private void AuthenticateUser(AuthorizationContext context)
        {
            var fam = FederatedAuthentication.WSFederationAuthenticationModule;
           
            var signIn = new SignInRequestMessage(new Uri(fam.Issuer), fam.Realm)
                             {
                                 Context = GetReturnUrl(context.RequestContext).ToString()
                             };
            context.Result = new RedirectResult(signIn.WriteQueryString());
        }

        private void AuthorizeUser(AuthorizationContext context)
        {
            if (!string.IsNullOrEmpty(Roles) && !context.HttpContext.User.IsInRole(Roles))
            {
                throw new HttpUnauthorizedException();    
            }
        }

        private Uri GetReturnUrl(RequestContext context)
        {
            var request = context.HttpContext.Request;
            var reqUrl = request.Url;
            var wreply = new StringBuilder();

            wreply.Append(reqUrl.Scheme);
            wreply.Append("://");
            wreply.Append(request.Headers["Host"] ?? reqUrl.Authority);
            wreply.Append(request.RawUrl);

            if (!request.ApplicationPath.EndsWith("/", StringComparison.OrdinalIgnoreCase))
            {
                wreply.Append("/");
            }

            return new Uri(wreply.ToString());
        }
    }
}