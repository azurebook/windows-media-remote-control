﻿using System;
using Castle.Windsor;
using CloudMedia.Remote.Framework.ServiceLocation;

namespace CloudMedia.Remote.Cloud.Web.UI.Configuration
{
    public interface IContainerBuilder : IDisposable
    {
        IServiceLocator Build();
        void BuildServices();
        void BuildInterceptors();
        void BuildControllers();
        void BuildCommands();
        IWindsorContainer Container { get; }
    }
}
