﻿using System;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Moq;
using CloudMedia.Remote.Cloud.Web.UI.Controllers;
using CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Cloud.Web.UI.Configuration
{
    public class DisconnectedContainerBuilder : DefaultContainerBuilder
    {
        private readonly Mock<IStatisticsService> _mockStatisticsService;

        public DisconnectedContainerBuilder()
        {
            _mockStatisticsService = new Mock<IStatisticsService>();
        }

        /// <summary>
        /// Change the default services configuration - for disconnected scenarios we do not want to execute against the Windows Azure Service Bus.
        /// </summary>
        public override void BuildServices()
        {
            var mockedResponse = new MediaCentreStatisticsResponse();
            mockedResponse.DomainName = "MOCK";
            mockedResponse.ExternalIP = "192.0.1.1";
            mockedResponse.LoggedOnUser = "MOCK";
            mockedResponse.MachineName = "MOCK";
            mockedResponse.OperatingSystemVersion = "6.1.1.0";
            mockedResponse.ProcessorCount = 2;
            mockedResponse.SystemDate = DateTime.Today;
            mockedResponse.SystemPageSize = 512;
            _mockStatisticsService.Setup(x => x.RetrieveStatistics()).Returns(mockedResponse);

            // now add this mocked service agent to the container.
            Container.Register(Component.For<IStatisticsService>().Instance(_mockStatisticsService.Object));
        }

        /// <summary>
        /// Change the interceptor to a mocked one - this saves us the need to authenticate with Access Control Service for federation.
        /// </summary>
        public override void BuildControllers()
        {
            Container.Register(Component.For<StatisticsController>()
                                .ImplementedBy<StatisticsController>()
                                .Interceptors(InterceptorReference.ForKey("MockedAuthInterceptor")).Anywhere
                                .LifeStyle.Transient);

            Container.Register(Component.For<AccountController>()
                                .ImplementedBy<AccountController>()
                                .Interceptors(InterceptorReference.ForKey("MockedAuthInterceptor")).Anywhere
                                .LifeStyle.Transient);

            Container.Register(Component.For<HomeController>()
                                .ImplementedBy<HomeController>()
                                .Interceptors(InterceptorReference.ForKey("MockedAuthInterceptor")).Anywhere
                                .LifeStyle.Transient);

            Container.Register(Component.For<GuideController>()
                                .ImplementedBy<GuideController>()
                                .Interceptors(InterceptorReference.ForKey("MockedAuthInterceptor")).Anywhere
                                .LifeStyle.Transient);
        }
    }
}