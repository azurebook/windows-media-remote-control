﻿using System;
using System.Web.Mvc;
using Castle.Core;
using Castle.DynamicProxy;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CloudMedia.Remote.Cloud.Web.UI.Controllers;
using CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands;
using CloudMedia.Remote.Cloud.Web.UI.Interceptors;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Framework.ServiceLocation;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Cloud.Web.UI.Configuration
{
    public class DefaultContainerBuilder : IContainerBuilder
    {
        private readonly IWindsorContainer _container;
        private WindsorServiceLocator _windsorServiceLocator;
        private bool _disposed;

        public DefaultContainerBuilder()
        {
            _container = new WindsorContainer();
            _container.AddFacility<WcfFacility>();

            // doc : http://stw.castleproject.org/(S(su2mgm45fclhqe55veo40545))/Windsor.Logging-Facility.ashx
        }

        /// <summary>
        /// Gets the current windsor container - required if you override the builders.
        /// </summary>
        public IWindsorContainer Container
        {
            get { return _container; }
        }

        public IServiceLocator Build()
        {
            BuildInterceptors();
            BuildControllers();
            BuildMvcPlumbing();
            BuildAndRegisterServiceLocator();
            BuildCommands();
            BuildServices();
            return _windsorServiceLocator;
        }

        public virtual void BuildInterceptors()
        {
            _container.Register(Component.For<IInterceptor>()
                                    .ImplementedBy<AuthorizationInterceptor>()
                                    .Named("AuthInterceptor"));

            _container.Register(Component.For<IInterceptor>()
                                   .ImplementedBy<MockedAuthorizationInterceptor>()
                                   .Named("MockedAuthInterceptor"));
        }

        public virtual void BuildControllers()
        {
            _container.Register(Component.For<StatisticsController>()
                                 .ImplementedBy<StatisticsController>()
                                 .Interceptors(InterceptorReference.ForKey("AuthInterceptor")).Anywhere
                                 .LifeStyle.Transient);

            _container.Register(Component.For<AccountController>()
                                .ImplementedBy<AccountController>()
                                .Interceptors(InterceptorReference.ForKey("AuthInterceptor")).Anywhere
                                .LifeStyle.Transient);

            _container.Register(Component.For<HomeController>()
                                .ImplementedBy<HomeController>()
                                .Interceptors(InterceptorReference.ForKey("AuthInterceptor")).Anywhere
                                .LifeStyle.Transient);

            _container.Register(Component.For<GuideController>()
                                .ImplementedBy<GuideController>()
                                .Interceptors(InterceptorReference.ForKey("AuthInterceptor")).Anywhere
                                .LifeStyle.Transient);
        }

        private void BuildMvcPlumbing()
        {
            var controllerFactory = new WindsorControllerFactory(_container.Kernel); 
            ControllerBuilder.Current.SetControllerFactory(controllerFactory); 
        }

        private void BuildAndRegisterServiceLocator()
        {
            _windsorServiceLocator = new WindsorServiceLocator(_container);
            ServiceLocator.SetLocatorProvider(() => _windsorServiceLocator);

            // now register the service locator with castle..
            _container.Register(Component.For<IServiceLocator>().Instance(_windsorServiceLocator));
        }

        public virtual void BuildCommands()
        {
            _container.Register(Component.For<ICommandAbstractFactory>()
                            .ImplementedBy<CommandAbstractFactory>()
                            .LifestyleSingleton());

            _container.Register(Component.For<ICommand<RetrieveMediaCentreStatisticsContext>>()
                            .ImplementedBy<RetrieveMediaCentreStatisticsCommand>()
                            .LifestyleTransient());

            _container.Register(Component.For<ICommand<GreetVisitorContext>>()
                            .ImplementedBy<GreetVisitorCommand>()
                            .LifestyleTransient());

            _container.Register(Component.For<ICommand<RecordTvShowContext>>()
                            .ImplementedBy<RecordTvShowCommand>()
                            .LifestyleTransient());


        }

        public virtual void BuildServices()
        {
            _container.Register(Component.For<IStatisticsService>()
                                    .AsWcfClient(
                                        DefaultClientModel.On(
                                            WcfEndpoint.FromConfiguration("CloudMedia.Remote.Desktop.Services.StatisticsService"))).LifestyleTransient());

            _container.Register(Component.For<ITelevisionService>()
                                   .AsWcfClient(
                                       DefaultClientModel.On(
                                           WcfEndpoint.FromConfiguration("CloudMedia.Remote.Desktop.Services.TelevisionService"))).LifestyleTransient());

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _container.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}