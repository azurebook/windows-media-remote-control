﻿using System;
using System.Text;
using CloudMedia.Remote.Cloud.Web.UI.Models;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands
{
    public class RecordTvShowCommand : BaseCommand<RecordTvShowContext>
    {
        private readonly ITelevisionService _televisionService;

        public RecordTvShowCommand(ITelevisionService televisionService)
        {
            _televisionService = televisionService;
        }

        protected override void DoExecute(RecordTvShowContext context)
        {
            context.GuideModel = new GuideModel();
            var tvShow = new StringBuilder();
            var date = DateTime.Now;
            tvShow.Append(string.Format("{0} {1}", date.ToShortDateString(), date.ToShortTimeString()));
            tvShow.Append(" ");
            tvShow.Append(Guid.NewGuid().ToString());
            _televisionService.Record(tvShow.ToString());
            context.GuideModel.StatusMessage = tvShow.ToString();
        }

        protected override ExceptionAction HandleError(RecordTvShowContext context, Exception exception)
        {
            return ExceptionAction.Rethrow;
        }
    }
}