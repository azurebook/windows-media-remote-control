﻿using CloudMedia.Remote.Cloud.Web.UI.Models;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands
{
    public class RecordTvShowContext
    {
        public GuideModel GuideModel { get; set; }
    }
}