﻿using System;
using System.Web.Mvc;
using CloudMedia.Remote.Cloud.Web.UI.Models;
using CloudMedia.Remote.Framework.Command;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands
{
    public class GreetVisitorCommand : BaseCommand<GreetVisitorContext>
    {
        protected override void DoExecute(GreetVisitorContext context)
        {
            context.HomeModel = new HomeModel();
            context.HomeModel.WelcomeMessage = context.Controller.ViewBag.Authorization.IsAuthenticated 
                ? new MvcHtmlString("Use the tabs along to top of the page to do things like, <span class=\"highlight\">view statistics, view TV guide, record TV shows.</span>") 
                : new MvcHtmlString("Before you can do anything, please authenticate yourself by clicking the <span class=\"highlight\">Log On</span> button in the top right corner of this page.");
        }

        protected override ExceptionAction HandleError(GreetVisitorContext context, Exception exception)
        {
            return ExceptionAction.Rethrow;
        }
    }
}