﻿using CloudMedia.Remote.Cloud.Web.UI.Models;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands
{
    public class GreetVisitorContext
    {
        private readonly SuperController _controller;

        public GreetVisitorContext(SuperController controller)
        {
            _controller = controller;
        }

        public SuperController Controller
        {
            get { return _controller; }
        }

        public HomeModel HomeModel
        {
            get;
            set;
        }
    }
}