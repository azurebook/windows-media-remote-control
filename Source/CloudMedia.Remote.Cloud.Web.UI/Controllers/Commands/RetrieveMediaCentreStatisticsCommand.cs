﻿using System;
using CloudMedia.Remote.Cloud.Web.UI.Models;
using CloudMedia.Remote.Framework;
using CloudMedia.Remote.Framework.Command;
using CloudMedia.Remote.Services.Contracts;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands
{
    public class RetrieveMediaCentreStatisticsCommand : BaseCommand<RetrieveMediaCentreStatisticsContext>
    {
        private readonly IStatisticsService _service;

        public RetrieveMediaCentreStatisticsCommand(IStatisticsService service)
        {
            _service = service;
        }

        protected override void DoExecute(RetrieveMediaCentreStatisticsContext context)
        {
            var response = _service.RetrieveStatistics();

            context.StatisticsModel = new StatisticsModel();
            context.StatisticsModel.MachineName = response.MachineName;
            context.StatisticsModel.OperatingSystemVersion = response.OperatingSystemVersion;
            var uptime = TimeSpan.FromMilliseconds(response.Uptime);
            context.StatisticsModel.Uptime = uptime.ToReadableString();
            context.StatisticsModel.LoggedOnUser = string.Format("{0}\\{1}", response.DomainName, response.LoggedOnUser);
            context.StatisticsModel.SystemPageSize = string.Format("{0:n0}", response.SystemPageSize);
            context.StatisticsModel.SystemDate = response.SystemDate;
            context.StatisticsModel.ProcessorCount = response.ProcessorCount;
            context.StatisticsModel.ExternalIP = response.ExternalIP;
        }

        protected override ExceptionAction HandleError(RetrieveMediaCentreStatisticsContext context, System.Exception exception)
        {
            return ExceptionAction.Rethrow;
        }
    }
}