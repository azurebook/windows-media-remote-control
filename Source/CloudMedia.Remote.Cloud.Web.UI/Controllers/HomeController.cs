﻿using System.Web.Mvc;
using CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands;
using CloudMedia.Remote.Framework.Command;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers
{
    public class HomeController : SuperController
    {
        private readonly ICommandAbstractFactory _commandAbstractFactory;

        public HomeController(ICommandAbstractFactory commandAbstractFactory)
        {
            _commandAbstractFactory = commandAbstractFactory;
        }

        public ActionResult Index()
        {
            // execute the greetvisitor command to create the home model.
            var context = new GreetVisitorContext(this);
            _commandAbstractFactory.ExecuteCommand(context);
            return View(context.HomeModel);
        }
    }
}
