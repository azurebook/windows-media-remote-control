﻿using System.ServiceModel;
using System.Web.Mvc;
using CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands;
using CloudMedia.Remote.Framework.Command;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers
{
    [HandleError(ExceptionType = typeof(EndpointNotFoundException), View = "ServiceBusNotAvailable")]
    [AuthenticateAndAuthorize(Roles = "Reader")]
    public class StatisticsController : SuperController
    {
        private readonly ICommandAbstractFactory _commandAbstractFactory;

        public StatisticsController(ICommandAbstractFactory commandAbstractFactory)
        {
            _commandAbstractFactory = commandAbstractFactory;
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Displays various information about the media centre being used.";
            var context = new RetrieveMediaCentreStatisticsContext();
            _commandAbstractFactory.ExecuteCommand(context);
        
            return View(context.StatisticsModel);
        }

    }
}
