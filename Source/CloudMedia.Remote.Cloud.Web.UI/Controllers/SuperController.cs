﻿using System.Web.Mvc;
using CloudMedia.Remote.Framework.Exceptions;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers
{
    [HandleError(ExceptionType = typeof(HttpUnauthorizedException), View = "AuthorizationError")]
    public class SuperController : Controller
    {
    }
}
