﻿using System;
using System.Web.Mvc;
using System.Web.UI;
using CloudMedia.Remote.Cloud.Web.UI.Controllers.Commands;
using CloudMedia.Remote.Cloud.Web.UI.Models;
using CloudMedia.Remote.Framework.Command;

namespace CloudMedia.Remote.Cloud.Web.UI.Controllers
{
    public class GuideController : SuperController
    {
        private readonly ICommandAbstractFactory _commandAbstractFactory;

        public GuideController(ICommandAbstractFactory commandAbstractFactory)
        {
            _commandAbstractFactory = commandAbstractFactory;
        }

        [AuthenticateAndAuthorize(Roles = "Reader")]
        [OutputCache(Duration = 300, Location = OutputCacheLocation.Any)]
        public ActionResult Index()
        {
            ViewBag.Message = ViewBag.Authorization.IsAdministrator ? "Here you can view the electronic programme guide and record TV shows" : "Here you can view the electronic programme guide";

            return View(new GuideModel());
        }

        [AuthenticateAndAuthorize(Roles = "Admin")]
        public ActionResult Record()
        {
            ViewBag.Message = ViewBag.Authorization.IsAdministrator ? "Here you can view the electronic programme guide and record TV shows" : "Here you can view the electronic programme guide";
            var context = new RecordTvShowContext();
            _commandAbstractFactory.ExecuteCommand(context);
            return View("Index", context.GuideModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your quintessential app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your quintessential contact page.";

            return View();
        }
    }
}
