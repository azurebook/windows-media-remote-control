﻿using System.ServiceModel;

namespace CloudMedia.Remote.Services.Contracts
{
    [ServiceContract(Name = "IStatisticsService", Namespace = "http://remotemedia.simonrhart.com/2011/12")]
    public interface IStatisticsService
    {
        [OperationContract]
        MediaCentreStatisticsResponse RetrieveStatistics();
    }

    public interface IStatisticsServiceChannel : IStatisticsService, IClientChannel { }
}
