﻿namespace CloudMedia.Remote.Services.Contracts
{
    public class ExternalIpResponse
    {
        public string ExternalIp { get; set; }
    }
}
