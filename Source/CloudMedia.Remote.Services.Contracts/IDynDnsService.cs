﻿namespace CloudMedia.Remote.Services.Contracts
{
    public interface IDynDnsService
    {
        ExternalIpResponse RetrieveExternalIp();
    }
}
