﻿using System;
using System.Runtime.Serialization;

namespace CloudMedia.Remote.Services.Contracts
{
    [DataContract]
    public class MediaCentreStatisticsResponse
    {
        /// <summary>
        /// Gets or sets the name of the machine running media centre remote control.
        /// </summary>
        [DataMember]
        public string MachineName { get; set; }

        [DataMember]
        public string OperatingSystemVersion { get; set; }

        /// <summary>
        /// Uptime in milliseconds.
        /// </summary>
        [DataMember]
        public int Uptime { get; set; }

        [DataMember]
        public string LoggedOnUser { get; set; }

        [DataMember]
        public string DomainName { get; set; }

        [DataMember]
        public int ProcessorCount { get; set; }

        [DataMember]
        public int SystemPageSize { get; set; }

        /// <summary>
        /// Gets or sets the date of the machine.
        /// </summary>
        [DataMember]
        public DateTime SystemDate { get; set; }

        [DataMember]
        public string ExternalIP { get; set; }
    }
}
