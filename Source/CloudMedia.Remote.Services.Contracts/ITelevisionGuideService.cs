﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace CloudMedia.Remote.Services.Contracts
{
    [ServiceContract(Name = "ITelevisionGuideService", Namespace = "http://remotemedia.simonrhart.com/2011/12")]
    public interface ITelevisionGuideService
    {
        /// <summary>
        /// Test operation to ensure the service is functioning OK.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [OperationContract]
        string Echo(string text);
    }

    public interface ITelevisionGuideServiceChannel : ITelevisionGuideService, IClientChannel { }
}
