﻿using System.ServiceModel;

namespace CloudMedia.Remote.Services.Contracts
{
    [ServiceContract(Name = "ITelevisionService", Namespace = "http://remotemedia.simonrhart.com/2011/12")]
    public interface ITelevisionService
    {
        [OperationContract(IsOneWay = true)]
        void Record(string text);
    }

    public interface ITelevisionServiceChannel : ITelevisionService, IClientChannel { }

}
