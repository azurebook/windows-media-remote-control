﻿using Smart421.Remote.Framework.ServiceLocation;

namespace Smart421.Remote.Cloud.Web.UI.Configuration
{
    public interface IContainerBuilder
    {
        IServiceLocator Build();
    }
}
