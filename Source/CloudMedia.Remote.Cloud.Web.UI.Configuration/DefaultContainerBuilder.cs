﻿using Castle.Windsor;
using Smart421.Remote.Framework.ServiceLocation;

namespace Smart421.Remote.Cloud.Web.UI.Configuration
{
    public class DefaultContainerBuilder : IContainerBuilder
    {
        private readonly IWindsorContainer _container;
       // private readonly DefaultServiceHostFactory _serviceHostFactory;
        private WindsorServiceLocator _windsorServiceLocator;
        private bool _disposed;

        public DefaultContainerBuilder()
        {
            _container = new WindsorContainer();
         //   _container.AddFacility<WcfFacility>();
            // doc : http://stw.castleproject.org/(S(su2mgm45fclhqe55veo40545))/Windsor.Logging-Facility.ashx

            // TO DO fix log 4 net - not working...
            //_container.AddFacility<LoggingFacility>(f => f.LogUsing(LoggerImplementation.Log4net).WithConfig("log4net.config")); 
           // _serviceHostFactory = new DefaultServiceHostFactory(_container.Kernel);
        }

        public IServiceLocator Build()
        {
            throw new System.NotImplementedException();
        }
    }
}