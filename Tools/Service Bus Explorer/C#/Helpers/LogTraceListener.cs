﻿#region Copyright
//=======================================================================================
//Microsoft Windows Server AppFabric Customer Advisory Team (CAT)  
//
// This sample is supplemental to the technical guidance published on the community
// blog at http://www.appfabriccat.com/. 
// 
// Author: Paolo Salvatori
//=======================================================================================
// Copyright © 2011 Microsoft Corporation. All rights reserved.
// 
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER 
// EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. YOU BEAR THE RISK OF USING IT.
//=======================================================================================
#endregion

#region Using Directives

using System.Diagnostics;

#endregion

namespace Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer
{
    public class LogTraceListener : TraceListener
    {
        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the LogTraceListener class.
        /// </summary>
        public LogTraceListener()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the LogTraceListener class.
        /// </summary>
        /// <param name="name">The name of the LogTraceListener.</param>
        public LogTraceListener(string name)
            : base(name)
        {
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Writes the specified message to the log ListBox on the UI of the MainForm.
        /// </summary>
        /// <param name="message">The message to write.</param>
        public override void Write(string message)
        {
            MainForm.StaticWriteToLog(message);
        }

        /// <summary>
        /// Writes the specified message to the log ListBox on the UI of the MainForm.
        /// </summary>
        /// <param name="message">The message to write.</param>
        public override void WriteLine(string message)
        {
            MainForm.StaticWriteToLog(message);
        }
        
        /// <summary>
        /// Emits an error message to the log ListBox on the UI of the MainForm.
        /// </summary>
        /// <param name="message">The error message to write.</param>
        public override void Fail(string message)
        {
            MainForm.StaticWriteToLog(message);
        }
        #endregion
    }
}
