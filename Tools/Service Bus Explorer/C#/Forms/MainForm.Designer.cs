﻿namespace Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparatorMain = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDefaultLayouToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.rootContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteEntityMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshRootMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.exportEntityMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importEntityMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.queuesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createQueueMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteQueuesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshQueuesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exportQueuesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ruleContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeRuleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rulesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addRuleMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteRulesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshRulesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subscriptionsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSubscriptionMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSubscriptionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshSubscriptionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.subscriptionContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeSubscriptionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testSubscriptionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshSubscriptionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.addRuleMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.copySubscriptionUrlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copySubscriptionDeadletterSubscriptionUrlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.topicContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteTopicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testTopicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshTopicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.exportTopicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addSubscriptionMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTopicSubscriptionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.copyTopicUrlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.queueContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteQueueMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testQueueMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshQueueMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exportQueueMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.copyQueueUrlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyQueueDeadletterQueueUrlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.topicsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createTopicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTopicsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshTopicsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exportTopicsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.relayServicesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshRelayServicesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.relayServiceContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyRelayServiceUrlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.queueFolderContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.folderCreateQueueMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderDeleteQueuesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.folderExportQueuesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.topicFolderContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.folderCreateTopicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderDeleteTopicsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.folderExportTopicsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.expandSubTreeMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.relayFolderContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.expandSubTreeMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseSubTreeMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.panelTreeView = new Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer.HeaderPanel();
            this.serviceBusTreeView = new System.Windows.Forms.TreeView();
            this.panelMain = new Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer.HeaderPanel();
            this.panelLog = new Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer.HeaderPanel();
            this.lstLog = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.rootContextMenuStrip.SuspendLayout();
            this.queuesContextMenuStrip.SuspendLayout();
            this.ruleContextMenuStrip.SuspendLayout();
            this.rulesContextMenuStrip.SuspendLayout();
            this.subscriptionsContextMenuStrip.SuspendLayout();
            this.subscriptionContextMenuStrip.SuspendLayout();
            this.topicContextMenuStrip.SuspendLayout();
            this.queueContextMenuStrip.SuspendLayout();
            this.topicsContextMenuStrip.SuspendLayout();
            this.relayServicesContextMenuStrip.SuspendLayout();
            this.relayServiceContextMenuStrip.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.queueFolderContextMenuStrip.SuspendLayout();
            this.topicFolderContextMenuStrip.SuspendLayout();
            this.relayFolderContextMenuStrip.SuspendLayout();
            this.panelTreeView.SuspendLayout();
            this.panelLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Queue.ico");
            this.imageList.Images.SetKeyName(1, "Topic.ico");
            this.imageList.Images.SetKeyName(2, "Chart.ico");
            this.imageList.Images.SetKeyName(3, "Class.ico");
            this.imageList.Images.SetKeyName(4, "Add.ico");
            this.imageList.Images.SetKeyName(5, "Contact.ico");
            this.imageList.Images.SetKeyName(6, "exec.ico");
            this.imageList.Images.SetKeyName(7, "Azure.ico");
            this.imageList.Images.SetKeyName(8, "World.png");
            this.imageList.Images.SetKeyName(9, "RelayService.png");
            this.imageList.Images.SetKeyName(10, "folder_web.ico");
            this.imageList.Images.SetKeyName(11, "Web.ico");
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToolStripMenuItem,
            this.toolStripSeparatorMain,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.connectToolStripMenuItem.Text = "&Connect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
            // 
            // toolStripSeparatorMain
            // 
            this.toolStripSeparatorMain.Name = "toolStripSeparatorMain";
            this.toolStripSeparatorMain.Size = new System.Drawing.Size(159, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.close_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearLogToolStripMenuItem,
            this.saveLogToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // clearLogToolStripMenuItem
            // 
            this.clearLogToolStripMenuItem.Name = "clearLogToolStripMenuItem";
            this.clearLogToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.clearLogToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.clearLogToolStripMenuItem.Text = "Clear Log";
            this.clearLogToolStripMenuItem.Click += new System.EventHandler(this.clearLog_Click);
            // 
            // saveLogToolStripMenuItem
            // 
            this.saveLogToolStripMenuItem.Name = "saveLogToolStripMenuItem";
            this.saveLogToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveLogToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveLogToolStripMenuItem.Text = "Save Log As...";
            this.saveLogToolStripMenuItem.Click += new System.EventHandler(this.saveLogToolStripMenuItem_Click);
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.actionsToolStripMenuItem.Text = "&Actions";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setDefaultLayouToolStripMenuItem,
            this.logWindowToolStripMenuItem,
            this.toolStripSeparator21,
            this.optionsToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // setDefaultLayouToolStripMenuItem
            // 
            this.setDefaultLayouToolStripMenuItem.Name = "setDefaultLayouToolStripMenuItem";
            this.setDefaultLayouToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.setDefaultLayouToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.setDefaultLayouToolStripMenuItem.Text = "Set Default Layout";
            this.setDefaultLayouToolStripMenuItem.Click += new System.EventHandler(this.setDefaultLayouToolStripMenuItem_Click);
            // 
            // logWindowToolStripMenuItem
            // 
            this.logWindowToolStripMenuItem.Checked = true;
            this.logWindowToolStripMenuItem.CheckOnClick = true;
            this.logWindowToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.logWindowToolStripMenuItem.Name = "logWindowToolStripMenuItem";
            this.logWindowToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.logWindowToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.logWindowToolStripMenuItem.Text = "&Log Window";
            this.logWindowToolStripMenuItem.Click += new System.EventHandler(this.logWindowToolStripMenuItem_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(209, 6);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.optionsToolStripMenuItem.Text = "Options...";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.aboutToolStripMenuItem.Text = "&About Service Bus Explorer";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.statusStrip.Location = new System.Drawing.Point(0, 780);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1248, 22);
            this.statusStrip.TabIndex = 19;
            this.statusStrip.Text = "statusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.panelTreeView);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.panelMain);
            this.splitContainer.Size = new System.Drawing.Size(1216, 370);
            this.splitContainer.SplitterDistance = 392;
            this.splitContainer.TabIndex = 1;
            this.splitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.mainSplitContainer_SplitterMoved);
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainSplitContainer.Location = new System.Drawing.Point(16, 40);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.splitContainer);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.panelLog);
            this.mainSplitContainer.Size = new System.Drawing.Size(1216, 744);
            this.mainSplitContainer.SplitterDistance = 370;
            this.mainSplitContainer.TabIndex = 21;
            this.mainSplitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.mainSplitContainer_SplitterMoved);
            // 
            // rootContextMenuStrip
            // 
            this.rootContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteEntityMenuItem,
            this.refreshRootMenuItem,
            this.toolStripSeparator9,
            this.exportEntityMenuItem,
            this.importEntityMenuItem,
            this.toolStripSeparator15,
            this.expandSubTreeMenuItem1,
            this.collapseSubTreeMenuItem1});
            this.rootContextMenuStrip.Name = "rootContextMenuStrip";
            this.rootContextMenuStrip.Size = new System.Drawing.Size(163, 148);
            // 
            // deleteEntityMenuItem
            // 
            this.deleteEntityMenuItem.Name = "deleteEntityMenuItem";
            this.deleteEntityMenuItem.Size = new System.Drawing.Size(162, 22);
            this.deleteEntityMenuItem.Text = "Delete Entities";
            this.deleteEntityMenuItem.ToolTipText = "Delete the entities contained in the current namespace.";
            this.deleteEntityMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // refreshRootMenuItem
            // 
            this.refreshRootMenuItem.Name = "refreshRootMenuItem";
            this.refreshRootMenuItem.Size = new System.Drawing.Size(162, 22);
            this.refreshRootMenuItem.Text = "Refresh Entities";
            this.refreshRootMenuItem.ToolTipText = "Refresh the entities contained in the current namespace.";
            this.refreshRootMenuItem.Click += new System.EventHandler(this.refreshEntityMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(159, 6);
            // 
            // exportEntityMenuItem
            // 
            this.exportEntityMenuItem.Name = "exportEntityMenuItem";
            this.exportEntityMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportEntityMenuItem.Text = "Export Entities";
            this.exportEntityMenuItem.ToolTipText = "Export entity definition to file.";
            this.exportEntityMenuItem.Click += new System.EventHandler(this.exportEntity_Click);
            // 
            // importEntityMenuItem
            // 
            this.importEntityMenuItem.Name = "importEntityMenuItem";
            this.importEntityMenuItem.Size = new System.Drawing.Size(162, 22);
            this.importEntityMenuItem.Text = "Import Entities";
            this.importEntityMenuItem.ToolTipText = "Import entity definition from file.";
            this.importEntityMenuItem.Click += new System.EventHandler(this.importEntity_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(159, 6);
            // 
            // expandSubTreeMenuItem1
            // 
            this.expandSubTreeMenuItem1.Name = "expandSubTreeMenuItem1";
            this.expandSubTreeMenuItem1.Size = new System.Drawing.Size(162, 22);
            this.expandSubTreeMenuItem1.Text = "Expand Subtree";
            this.expandSubTreeMenuItem1.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem1.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem1
            // 
            this.collapseSubTreeMenuItem1.Name = "collapseSubTreeMenuItem1";
            this.collapseSubTreeMenuItem1.Size = new System.Drawing.Size(162, 22);
            this.collapseSubTreeMenuItem1.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem1.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem1.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // queuesContextMenuStrip
            // 
            this.queuesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createQueueMenuItem,
            this.deleteQueuesMenuItem,
            this.refreshQueuesMenuItem,
            this.toolStripSeparator3,
            this.exportQueuesMenuItem,
            this.toolStripSeparator14,
            this.expandSubTreeMenuItem2,
            this.collapseSubTreeMenuItem2});
            this.queuesContextMenuStrip.Name = "createContextMenuStrip";
            this.queuesContextMenuStrip.Size = new System.Drawing.Size(163, 148);
            // 
            // createQueueMenuItem
            // 
            this.createQueueMenuItem.Name = "createQueueMenuItem";
            this.createQueueMenuItem.Size = new System.Drawing.Size(162, 22);
            this.createQueueMenuItem.Text = "Create Queue";
            this.createQueueMenuItem.ToolTipText = "Create a new queue.";
            this.createQueueMenuItem.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // deleteQueuesMenuItem
            // 
            this.deleteQueuesMenuItem.Name = "deleteQueuesMenuItem";
            this.deleteQueuesMenuItem.Size = new System.Drawing.Size(162, 22);
            this.deleteQueuesMenuItem.Text = "Delete Queues";
            this.deleteQueuesMenuItem.ToolTipText = "Deletes all queues in the current namespace.";
            this.deleteQueuesMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // refreshQueuesMenuItem
            // 
            this.refreshQueuesMenuItem.Name = "refreshQueuesMenuItem";
            this.refreshQueuesMenuItem.Size = new System.Drawing.Size(162, 22);
            this.refreshQueuesMenuItem.Text = "Refresh Queues";
            this.refreshQueuesMenuItem.ToolTipText = "Refresh all queues in the current namespace.";
            this.refreshQueuesMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(159, 6);
            // 
            // exportQueuesMenuItem
            // 
            this.exportQueuesMenuItem.Name = "exportQueuesMenuItem";
            this.exportQueuesMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportQueuesMenuItem.Text = "Export Queues";
            this.exportQueuesMenuItem.ToolTipText = "Export queues definition to file.";
            this.exportQueuesMenuItem.Click += new System.EventHandler(this.exportEntity_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(159, 6);
            // 
            // expandSubTreeMenuItem2
            // 
            this.expandSubTreeMenuItem2.Name = "expandSubTreeMenuItem2";
            this.expandSubTreeMenuItem2.Size = new System.Drawing.Size(162, 22);
            this.expandSubTreeMenuItem2.Text = "Expand Subtree";
            this.expandSubTreeMenuItem2.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem2.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem2
            // 
            this.collapseSubTreeMenuItem2.Name = "collapseSubTreeMenuItem2";
            this.collapseSubTreeMenuItem2.Size = new System.Drawing.Size(162, 22);
            this.collapseSubTreeMenuItem2.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem2.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem2.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // ruleContextMenuStrip
            // 
            this.ruleContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeRuleMenuItem});
            this.ruleContextMenuStrip.Name = "ruleContextMenuStrip";
            this.ruleContextMenuStrip.Size = new System.Drawing.Size(144, 26);
            // 
            // removeRuleMenuItem
            // 
            this.removeRuleMenuItem.Name = "removeRuleMenuItem";
            this.removeRuleMenuItem.Size = new System.Drawing.Size(143, 22);
            this.removeRuleMenuItem.Text = "Remove Rule";
            this.removeRuleMenuItem.ToolTipText = "Remove the current rule.";
            this.removeRuleMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // rulesContextMenuStrip
            // 
            this.rulesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addRuleMenuItem2,
            this.deleteRulesMenuItem,
            this.toolStripSeparator10,
            this.refreshRulesMenuItem});
            this.rulesContextMenuStrip.Name = "rulesContextMenuStrip";
            this.rulesContextMenuStrip.Size = new System.Drawing.Size(145, 76);
            // 
            // addRuleMenuItem2
            // 
            this.addRuleMenuItem2.Name = "addRuleMenuItem2";
            this.addRuleMenuItem2.Size = new System.Drawing.Size(144, 22);
            this.addRuleMenuItem2.Text = "Add Rule";
            this.addRuleMenuItem2.ToolTipText = "Add a new rule.";
            this.addRuleMenuItem2.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // deleteRulesMenuItem
            // 
            this.deleteRulesMenuItem.Name = "deleteRulesMenuItem";
            this.deleteRulesMenuItem.Size = new System.Drawing.Size(144, 22);
            this.deleteRulesMenuItem.Text = "Delete Rules";
            this.deleteRulesMenuItem.ToolTipText = "Delete rules for the current subscription.";
            this.deleteRulesMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(141, 6);
            // 
            // refreshRulesMenuItem
            // 
            this.refreshRulesMenuItem.Name = "refreshRulesMenuItem";
            this.refreshRulesMenuItem.Size = new System.Drawing.Size(144, 22);
            this.refreshRulesMenuItem.Text = "Refresh Rules";
            this.refreshRulesMenuItem.ToolTipText = "Refresh rules for the current subscription.";
            this.refreshRulesMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // subscriptionsContextMenuStrip
            // 
            this.subscriptionsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSubscriptionMenuItem2,
            this.deleteSubscriptionsMenuItem,
            this.refreshSubscriptionsMenuItem,
            this.toolStripSeparator19,
            this.expandSubTreeMenuItem6,
            this.collapseSubTreeMenuItem6});
            this.subscriptionsContextMenuStrip.Name = "subscriptionsContextMenuStrip";
            this.subscriptionsContextMenuStrip.Size = new System.Drawing.Size(188, 120);
            // 
            // addSubscriptionMenuItem2
            // 
            this.addSubscriptionMenuItem2.Name = "addSubscriptionMenuItem2";
            this.addSubscriptionMenuItem2.Size = new System.Drawing.Size(187, 22);
            this.addSubscriptionMenuItem2.Text = "Create Subscription";
            this.addSubscriptionMenuItem2.ToolTipText = "Add a new subscription.";
            this.addSubscriptionMenuItem2.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // deleteSubscriptionsMenuItem
            // 
            this.deleteSubscriptionsMenuItem.Name = "deleteSubscriptionsMenuItem";
            this.deleteSubscriptionsMenuItem.Size = new System.Drawing.Size(187, 22);
            this.deleteSubscriptionsMenuItem.Text = "Delete Subscriptions";
            this.deleteSubscriptionsMenuItem.ToolTipText = "Delete all subscription for the current topic.";
            this.deleteSubscriptionsMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // refreshSubscriptionsMenuItem
            // 
            this.refreshSubscriptionsMenuItem.Name = "refreshSubscriptionsMenuItem";
            this.refreshSubscriptionsMenuItem.Size = new System.Drawing.Size(187, 22);
            this.refreshSubscriptionsMenuItem.Text = "Refresh Subscriptions";
            this.refreshSubscriptionsMenuItem.ToolTipText = "Refresh subscriptions for the current topic.";
            this.refreshSubscriptionsMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(184, 6);
            // 
            // expandSubTreeMenuItem6
            // 
            this.expandSubTreeMenuItem6.Name = "expandSubTreeMenuItem6";
            this.expandSubTreeMenuItem6.Size = new System.Drawing.Size(187, 22);
            this.expandSubTreeMenuItem6.Text = "Expand Subtree";
            this.expandSubTreeMenuItem6.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem6.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem6
            // 
            this.collapseSubTreeMenuItem6.Name = "collapseSubTreeMenuItem6";
            this.collapseSubTreeMenuItem6.Size = new System.Drawing.Size(187, 22);
            this.collapseSubTreeMenuItem6.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem6.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem6.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // subscriptionContextMenuStrip
            // 
            this.subscriptionContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeSubscriptionMenuItem,
            this.testSubscriptionMenuItem,
            this.refreshSubscriptionMenuItem,
            this.toolStripSeparator7,
            this.addRuleMenuItem1,
            this.toolStripSeparator8,
            this.copySubscriptionUrlMenuItem,
            this.copySubscriptionDeadletterSubscriptionUrlMenuItem,
            this.toolStripSeparator20,
            this.expandSubTreeMenuItem7,
            this.collapseSubTreeMenuItem7});
            this.subscriptionContextMenuStrip.Name = "subscriptionContextMenuStrip";
            this.subscriptionContextMenuStrip.Size = new System.Drawing.Size(216, 198);
            // 
            // removeSubscriptionMenuItem
            // 
            this.removeSubscriptionMenuItem.Name = "removeSubscriptionMenuItem";
            this.removeSubscriptionMenuItem.Size = new System.Drawing.Size(215, 22);
            this.removeSubscriptionMenuItem.Text = "Delete Subscription";
            this.removeSubscriptionMenuItem.ToolTipText = "Delete the current subscription.";
            this.removeSubscriptionMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // testSubscriptionMenuItem
            // 
            this.testSubscriptionMenuItem.Name = "testSubscriptionMenuItem";
            this.testSubscriptionMenuItem.Size = new System.Drawing.Size(215, 22);
            this.testSubscriptionMenuItem.Text = "Test Subscription";
            this.testSubscriptionMenuItem.ToolTipText = "Test the current subscription.";
            this.testSubscriptionMenuItem.Click += new System.EventHandler(this.testEntity_Click);
            // 
            // refreshSubscriptionMenuItem
            // 
            this.refreshSubscriptionMenuItem.Name = "refreshSubscriptionMenuItem";
            this.refreshSubscriptionMenuItem.Size = new System.Drawing.Size(215, 22);
            this.refreshSubscriptionMenuItem.Text = "Refresh Subscription";
            this.refreshSubscriptionMenuItem.ToolTipText = "Refresh the current subscription.";
            this.refreshSubscriptionMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(212, 6);
            // 
            // addRuleMenuItem1
            // 
            this.addRuleMenuItem1.Name = "addRuleMenuItem1";
            this.addRuleMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.addRuleMenuItem1.Text = "Add Rule";
            this.addRuleMenuItem1.ToolTipText = "Add a new rule.";
            this.addRuleMenuItem1.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(212, 6);
            // 
            // copySubscriptionUrlMenuItem
            // 
            this.copySubscriptionUrlMenuItem.Name = "copySubscriptionUrlMenuItem";
            this.copySubscriptionUrlMenuItem.Size = new System.Drawing.Size(215, 22);
            this.copySubscriptionUrlMenuItem.Text = "Copy Subscription Url";
            this.copySubscriptionUrlMenuItem.ToolTipText = "Copy the subscription url to the clipboard.";
            this.copySubscriptionUrlMenuItem.Click += new System.EventHandler(this.copyEntityUrl_Click);
            // 
            // copySubscriptionDeadletterSubscriptionUrlMenuItem
            // 
            this.copySubscriptionDeadletterSubscriptionUrlMenuItem.Name = "copySubscriptionDeadletterSubscriptionUrlMenuItem";
            this.copySubscriptionDeadletterSubscriptionUrlMenuItem.Size = new System.Drawing.Size(215, 22);
            this.copySubscriptionDeadletterSubscriptionUrlMenuItem.Text = "Copy Deadletter Queue Url";
            this.copySubscriptionDeadletterSubscriptionUrlMenuItem.ToolTipText = "Copy the deadletter queue url to the clipboard.";
            this.copySubscriptionDeadletterSubscriptionUrlMenuItem.Click += new System.EventHandler(this.copyEntityUrl_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(212, 6);
            // 
            // expandSubTreeMenuItem7
            // 
            this.expandSubTreeMenuItem7.Name = "expandSubTreeMenuItem7";
            this.expandSubTreeMenuItem7.Size = new System.Drawing.Size(215, 22);
            this.expandSubTreeMenuItem7.Text = "Expand Subtree";
            this.expandSubTreeMenuItem7.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem7.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem7
            // 
            this.collapseSubTreeMenuItem7.Name = "collapseSubTreeMenuItem7";
            this.collapseSubTreeMenuItem7.Size = new System.Drawing.Size(215, 22);
            this.collapseSubTreeMenuItem7.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem7.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem7.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // topicContextMenuStrip
            // 
            this.topicContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteTopicMenuItem,
            this.testTopicMenuItem,
            this.refreshTopicMenuItem,
            this.toolStripSeparator6,
            this.exportTopicMenuItem,
            this.toolStripSeparator2,
            this.addSubscriptionMenuItem1,
            this.deleteTopicSubscriptionsMenuItem,
            this.toolStripSeparator12,
            this.copyTopicUrlMenuItem,
            this.toolStripSeparator18,
            this.expandSubTreeMenuItem5,
            this.collapseSubTreeMenuItem5});
            this.topicContextMenuStrip.Name = "topicContextMenuStrip";
            this.topicContextMenuStrip.Size = new System.Drawing.Size(182, 226);
            // 
            // deleteTopicMenuItem
            // 
            this.deleteTopicMenuItem.Name = "deleteTopicMenuItem";
            this.deleteTopicMenuItem.Size = new System.Drawing.Size(181, 22);
            this.deleteTopicMenuItem.Text = "Delete Topic";
            this.deleteTopicMenuItem.ToolTipText = "Delete the current topic.";
            this.deleteTopicMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // testTopicMenuItem
            // 
            this.testTopicMenuItem.Name = "testTopicMenuItem";
            this.testTopicMenuItem.Size = new System.Drawing.Size(181, 22);
            this.testTopicMenuItem.Text = "Test Topic";
            this.testTopicMenuItem.ToolTipText = "Test the current topic.";
            this.testTopicMenuItem.Click += new System.EventHandler(this.testEntity_Click);
            // 
            // refreshTopicMenuItem
            // 
            this.refreshTopicMenuItem.Name = "refreshTopicMenuItem";
            this.refreshTopicMenuItem.Size = new System.Drawing.Size(181, 22);
            this.refreshTopicMenuItem.Text = "Refresh Topic";
            this.refreshTopicMenuItem.ToolTipText = "Refresh the current topic.";
            this.refreshTopicMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(178, 6);
            // 
            // exportTopicMenuItem
            // 
            this.exportTopicMenuItem.Name = "exportTopicMenuItem";
            this.exportTopicMenuItem.Size = new System.Drawing.Size(181, 22);
            this.exportTopicMenuItem.Text = "Export Topic";
            this.exportTopicMenuItem.ToolTipText = "Export topic definition to file.";
            this.exportTopicMenuItem.Click += new System.EventHandler(this.exportEntity_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(178, 6);
            // 
            // addSubscriptionMenuItem1
            // 
            this.addSubscriptionMenuItem1.Name = "addSubscriptionMenuItem1";
            this.addSubscriptionMenuItem1.Size = new System.Drawing.Size(181, 22);
            this.addSubscriptionMenuItem1.Text = "Create Subscription";
            this.addSubscriptionMenuItem1.ToolTipText = "Create a new subscription to the current topic.";
            this.addSubscriptionMenuItem1.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // deleteTopicSubscriptionsMenuItem
            // 
            this.deleteTopicSubscriptionsMenuItem.Name = "deleteTopicSubscriptionsMenuItem";
            this.deleteTopicSubscriptionsMenuItem.Size = new System.Drawing.Size(181, 22);
            this.deleteTopicSubscriptionsMenuItem.Text = "Delete Subscriptions";
            this.deleteTopicSubscriptionsMenuItem.ToolTipText = "Delete all subscription for the current topic.";
            this.deleteTopicSubscriptionsMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(178, 6);
            // 
            // copyTopicUrlMenuItem
            // 
            this.copyTopicUrlMenuItem.Name = "copyTopicUrlMenuItem";
            this.copyTopicUrlMenuItem.Size = new System.Drawing.Size(181, 22);
            this.copyTopicUrlMenuItem.Text = "Copy Topic Url";
            this.copyTopicUrlMenuItem.ToolTipText = "Copy the topic url to the clipboard.";
            this.copyTopicUrlMenuItem.Click += new System.EventHandler(this.copyEntityUrl_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(178, 6);
            // 
            // expandSubTreeMenuItem5
            // 
            this.expandSubTreeMenuItem5.Name = "expandSubTreeMenuItem5";
            this.expandSubTreeMenuItem5.Size = new System.Drawing.Size(181, 22);
            this.expandSubTreeMenuItem5.Text = "Expand Subtree";
            this.expandSubTreeMenuItem5.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem5.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem5
            // 
            this.collapseSubTreeMenuItem5.Name = "collapseSubTreeMenuItem5";
            this.collapseSubTreeMenuItem5.Size = new System.Drawing.Size(181, 22);
            this.collapseSubTreeMenuItem5.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem5.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem5.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // queueContextMenuStrip
            // 
            this.queueContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteQueueMenuItem,
            this.testQueueMenuItem,
            this.refreshQueueMenuItem,
            this.toolStripSeparator5,
            this.exportQueueMenuItem,
            this.toolStripSeparator11,
            this.copyQueueUrlMenuItem,
            this.copyQueueDeadletterQueueUrlMenuItem,
            this.toolStripSeparator17,
            this.expandSubTreeMenuItem4,
            this.collapseSubTreeMenuItem4});
            this.queueContextMenuStrip.Name = "nodeContextMenuStrip";
            this.queueContextMenuStrip.Size = new System.Drawing.Size(216, 198);
            // 
            // deleteQueueMenuItem
            // 
            this.deleteQueueMenuItem.Name = "deleteQueueMenuItem";
            this.deleteQueueMenuItem.Size = new System.Drawing.Size(215, 22);
            this.deleteQueueMenuItem.Text = "Delete Queue";
            this.deleteQueueMenuItem.ToolTipText = "Delete the current queue.";
            this.deleteQueueMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // testQueueMenuItem
            // 
            this.testQueueMenuItem.Name = "testQueueMenuItem";
            this.testQueueMenuItem.Size = new System.Drawing.Size(215, 22);
            this.testQueueMenuItem.Text = "Test Queue";
            this.testQueueMenuItem.ToolTipText = "Test the current queue.";
            this.testQueueMenuItem.Click += new System.EventHandler(this.testEntity_Click);
            // 
            // refreshQueueMenuItem
            // 
            this.refreshQueueMenuItem.Name = "refreshQueueMenuItem";
            this.refreshQueueMenuItem.Size = new System.Drawing.Size(215, 22);
            this.refreshQueueMenuItem.Text = "Refresh Queue";
            this.refreshQueueMenuItem.ToolTipText = "Refresh the current queue.";
            this.refreshQueueMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(212, 6);
            // 
            // exportQueueMenuItem
            // 
            this.exportQueueMenuItem.Name = "exportQueueMenuItem";
            this.exportQueueMenuItem.Size = new System.Drawing.Size(215, 22);
            this.exportQueueMenuItem.Text = "Export Queue";
            this.exportQueueMenuItem.ToolTipText = "Export queue definition to file.";
            this.exportQueueMenuItem.Click += new System.EventHandler(this.exportEntity_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(212, 6);
            // 
            // copyQueueUrlMenuItem
            // 
            this.copyQueueUrlMenuItem.Name = "copyQueueUrlMenuItem";
            this.copyQueueUrlMenuItem.Size = new System.Drawing.Size(215, 22);
            this.copyQueueUrlMenuItem.Text = "Copy Queue Url";
            this.copyQueueUrlMenuItem.ToolTipText = "Copy the queue url to the clipboard.";
            this.copyQueueUrlMenuItem.Click += new System.EventHandler(this.copyEntityUrl_Click);
            // 
            // copyQueueDeadletterQueueUrlMenuItem
            // 
            this.copyQueueDeadletterQueueUrlMenuItem.Name = "copyQueueDeadletterQueueUrlMenuItem";
            this.copyQueueDeadletterQueueUrlMenuItem.Size = new System.Drawing.Size(215, 22);
            this.copyQueueDeadletterQueueUrlMenuItem.Text = "Copy Deadletter Queue Url";
            this.copyQueueDeadletterQueueUrlMenuItem.ToolTipText = "Copy the deadletter queue url to the clipboard.";
            this.copyQueueDeadletterQueueUrlMenuItem.Click += new System.EventHandler(this.copyEntityUrl_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(212, 6);
            // 
            // expandSubTreeMenuItem4
            // 
            this.expandSubTreeMenuItem4.Name = "expandSubTreeMenuItem4";
            this.expandSubTreeMenuItem4.Size = new System.Drawing.Size(215, 22);
            this.expandSubTreeMenuItem4.Text = "Expand Subtree";
            this.expandSubTreeMenuItem4.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem4.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem4
            // 
            this.collapseSubTreeMenuItem4.Name = "collapseSubTreeMenuItem4";
            this.collapseSubTreeMenuItem4.Size = new System.Drawing.Size(215, 22);
            this.collapseSubTreeMenuItem4.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem4.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem4.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // topicsContextMenuStrip
            // 
            this.topicsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTopicMenuItem,
            this.deleteTopicsMenuItem,
            this.refreshTopicsMenuItem,
            this.toolStripSeparator4,
            this.exportTopicsMenuItem,
            this.toolStripSeparator16,
            this.expandSubTreeMenuItem3,
            this.collapseSubTreeMenuItem3});
            this.topicsContextMenuStrip.Name = "createContextMenuStrip";
            this.topicsContextMenuStrip.Size = new System.Drawing.Size(163, 148);
            // 
            // createTopicMenuItem
            // 
            this.createTopicMenuItem.Name = "createTopicMenuItem";
            this.createTopicMenuItem.Size = new System.Drawing.Size(162, 22);
            this.createTopicMenuItem.Text = "Create Topic";
            this.createTopicMenuItem.ToolTipText = "Create a new topic.";
            this.createTopicMenuItem.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // deleteTopicsMenuItem
            // 
            this.deleteTopicsMenuItem.Name = "deleteTopicsMenuItem";
            this.deleteTopicsMenuItem.Size = new System.Drawing.Size(162, 22);
            this.deleteTopicsMenuItem.Text = "Delete Topics";
            this.deleteTopicsMenuItem.ToolTipText = "Delete all topics in the current namespace.";
            this.deleteTopicsMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // refreshTopicsMenuItem
            // 
            this.refreshTopicsMenuItem.Name = "refreshTopicsMenuItem";
            this.refreshTopicsMenuItem.Size = new System.Drawing.Size(162, 22);
            this.refreshTopicsMenuItem.Text = "Refresh Topics";
            this.refreshTopicsMenuItem.ToolTipText = "Refresh all topics in the current namespace.";
            this.refreshTopicsMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(159, 6);
            // 
            // exportTopicsMenuItem
            // 
            this.exportTopicsMenuItem.Name = "exportTopicsMenuItem";
            this.exportTopicsMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportTopicsMenuItem.Text = "Export Topics";
            this.exportTopicsMenuItem.ToolTipText = "Export topics definition to file.";
            this.exportTopicsMenuItem.Click += new System.EventHandler(this.exportEntity_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(159, 6);
            // 
            // expandSubTreeMenuItem3
            // 
            this.expandSubTreeMenuItem3.Name = "expandSubTreeMenuItem3";
            this.expandSubTreeMenuItem3.Size = new System.Drawing.Size(162, 22);
            this.expandSubTreeMenuItem3.Text = "Expand Subtree";
            this.expandSubTreeMenuItem3.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem3.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem3
            // 
            this.collapseSubTreeMenuItem3.Name = "collapseSubTreeMenuItem3";
            this.collapseSubTreeMenuItem3.Size = new System.Drawing.Size(162, 22);
            this.collapseSubTreeMenuItem3.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem3.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem3.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // relayServicesContextMenuStrip
            // 
            this.relayServicesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshRelayServicesMenuItem,
            this.toolStripSeparator24,
            this.expandSubTreeMenuItem11,
            this.collapseSubTreeMenuItem11});
            this.relayServicesContextMenuStrip.Name = "relayServicesContextMenuStrip";
            this.relayServicesContextMenuStrip.Size = new System.Drawing.Size(190, 76);
            // 
            // refreshRelayServicesMenuItem
            // 
            this.refreshRelayServicesMenuItem.Name = "refreshRelayServicesMenuItem";
            this.refreshRelayServicesMenuItem.Size = new System.Drawing.Size(189, 22);
            this.refreshRelayServicesMenuItem.Text = "Refresh Relay Services";
            this.refreshRelayServicesMenuItem.ToolTipText = "Refresh all relay services in the current namespace.";
            this.refreshRelayServicesMenuItem.Click += new System.EventHandler(this.refreshEntity_Click);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(186, 6);
            // 
            // expandSubTreeMenuItem11
            // 
            this.expandSubTreeMenuItem11.Name = "expandSubTreeMenuItem11";
            this.expandSubTreeMenuItem11.Size = new System.Drawing.Size(189, 22);
            this.expandSubTreeMenuItem11.Text = "Expand Subtree";
            this.expandSubTreeMenuItem11.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem11.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem11
            // 
            this.collapseSubTreeMenuItem11.Name = "collapseSubTreeMenuItem11";
            this.collapseSubTreeMenuItem11.Size = new System.Drawing.Size(189, 22);
            this.collapseSubTreeMenuItem11.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem11.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem11.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // relayServiceContextMenuStrip
            // 
            this.relayServiceContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyRelayServiceUrlMenuItem});
            this.relayServiceContextMenuStrip.Name = "subscriptionContextMenuStrip";
            this.relayServiceContextMenuStrip.Size = new System.Drawing.Size(192, 48);
            // 
            // copyRelayServiceUrlMenuItem
            // 
            this.copyRelayServiceUrlMenuItem.Name = "copyRelayServiceUrlMenuItem";
            this.copyRelayServiceUrlMenuItem.Size = new System.Drawing.Size(191, 22);
            this.copyRelayServiceUrlMenuItem.Text = "Copy Relay Service Url";
            this.copyRelayServiceUrlMenuItem.ToolTipText = "Copy the relay service url to the clipboard.";
            this.copyRelayServiceUrlMenuItem.Click += new System.EventHandler(this.copyEntityUrl_Click);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.BackColor = System.Drawing.Color.Transparent;
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.actionsToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1248, 24);
            this.mainMenuStrip.TabIndex = 22;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.logoPictureBox.BackgroundImage = global::Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer.Properties.Resources.WindowsAzureSmall;
            this.logoPictureBox.Location = new System.Drawing.Point(1120, 6);
            this.logoPictureBox.Name = "logoPictureBox";
            this.logoPictureBox.Size = new System.Drawing.Size(110, 24);
            this.logoPictureBox.TabIndex = 23;
            this.logoPictureBox.TabStop = false;
            // 
            // queueFolderContextMenuStrip
            // 
            this.queueFolderContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.folderCreateQueueMenuItem,
            this.folderDeleteQueuesMenuItem,
            this.toolStripSeparator1,
            this.folderExportQueuesMenuItem,
            this.toolStripSeparator22,
            this.expandSubTreeMenuItem9,
            this.collapseSubTreeMenuItem9});
            this.queueFolderContextMenuStrip.Name = "createContextMenuStrip";
            this.queueFolderContextMenuStrip.Size = new System.Drawing.Size(163, 126);
            // 
            // folderCreateQueueMenuItem
            // 
            this.folderCreateQueueMenuItem.Name = "folderCreateQueueMenuItem";
            this.folderCreateQueueMenuItem.Size = new System.Drawing.Size(162, 22);
            this.folderCreateQueueMenuItem.Text = "Create Queue";
            this.folderCreateQueueMenuItem.ToolTipText = "Create a new queue in the current path.";
            this.folderCreateQueueMenuItem.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // folderDeleteQueuesMenuItem
            // 
            this.folderDeleteQueuesMenuItem.Name = "folderDeleteQueuesMenuItem";
            this.folderDeleteQueuesMenuItem.Size = new System.Drawing.Size(162, 22);
            this.folderDeleteQueuesMenuItem.Text = "Delete Queues";
            this.folderDeleteQueuesMenuItem.ToolTipText = "Deletes all queues in the current path.";
            this.folderDeleteQueuesMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(159, 6);
            // 
            // folderExportQueuesMenuItem
            // 
            this.folderExportQueuesMenuItem.Name = "folderExportQueuesMenuItem";
            this.folderExportQueuesMenuItem.Size = new System.Drawing.Size(162, 22);
            this.folderExportQueuesMenuItem.Text = "Export Queues";
            this.folderExportQueuesMenuItem.ToolTipText = "Export the definition of the queues in the current path to file.";
            this.folderExportQueuesMenuItem.Click += new System.EventHandler(this.exportEntity_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(159, 6);
            // 
            // expandSubTreeMenuItem9
            // 
            this.expandSubTreeMenuItem9.Name = "expandSubTreeMenuItem9";
            this.expandSubTreeMenuItem9.Size = new System.Drawing.Size(162, 22);
            this.expandSubTreeMenuItem9.Text = "Expand Subtree";
            this.expandSubTreeMenuItem9.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem9.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem9
            // 
            this.collapseSubTreeMenuItem9.Name = "collapseSubTreeMenuItem9";
            this.collapseSubTreeMenuItem9.Size = new System.Drawing.Size(162, 22);
            this.collapseSubTreeMenuItem9.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem9.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem9.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // topicFolderContextMenuStrip
            // 
            this.topicFolderContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.folderCreateTopicMenuItem,
            this.folderDeleteTopicsMenuItem,
            this.toolStripSeparator13,
            this.folderExportTopicsMenuItem,
            this.toolStripSeparator23,
            this.expandSubTreeMenuItem10,
            this.collapseSubTreeMenuItem10});
            this.topicFolderContextMenuStrip.Name = "createContextMenuStrip";
            this.topicFolderContextMenuStrip.Size = new System.Drawing.Size(163, 126);
            // 
            // folderCreateTopicMenuItem
            // 
            this.folderCreateTopicMenuItem.Name = "folderCreateTopicMenuItem";
            this.folderCreateTopicMenuItem.Size = new System.Drawing.Size(162, 22);
            this.folderCreateTopicMenuItem.Text = "Create Topic";
            this.folderCreateTopicMenuItem.ToolTipText = "Create a new topic in the specified path.";
            this.folderCreateTopicMenuItem.Click += new System.EventHandler(this.createEntity_Click);
            // 
            // folderDeleteTopicsMenuItem
            // 
            this.folderDeleteTopicsMenuItem.Name = "folderDeleteTopicsMenuItem";
            this.folderDeleteTopicsMenuItem.Size = new System.Drawing.Size(162, 22);
            this.folderDeleteTopicsMenuItem.Text = "Delete Topics";
            this.folderDeleteTopicsMenuItem.ToolTipText = "Delete all topics in the current path.";
            this.folderDeleteTopicsMenuItem.Click += new System.EventHandler(this.deleteEntity_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(159, 6);
            // 
            // folderExportTopicsMenuItem
            // 
            this.folderExportTopicsMenuItem.Name = "folderExportTopicsMenuItem";
            this.folderExportTopicsMenuItem.Size = new System.Drawing.Size(162, 22);
            this.folderExportTopicsMenuItem.Text = "Export Topics";
            this.folderExportTopicsMenuItem.ToolTipText = "Export the definition of the topics in the current path to file.";
            this.folderExportTopicsMenuItem.Click += new System.EventHandler(this.exportEntity_Click);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(159, 6);
            // 
            // expandSubTreeMenuItem10
            // 
            this.expandSubTreeMenuItem10.Name = "expandSubTreeMenuItem10";
            this.expandSubTreeMenuItem10.Size = new System.Drawing.Size(162, 22);
            this.expandSubTreeMenuItem10.Text = "Expand Subtree";
            this.expandSubTreeMenuItem10.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem10.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem10
            // 
            this.collapseSubTreeMenuItem10.Name = "collapseSubTreeMenuItem10";
            this.collapseSubTreeMenuItem10.Size = new System.Drawing.Size(162, 22);
            this.collapseSubTreeMenuItem10.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem10.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem10.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // relayFolderContextMenuStrip
            // 
            this.relayFolderContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.expandSubTreeMenuItem12,
            this.collapseSubTreeMenuItem12});
            this.relayFolderContextMenuStrip.Name = "createContextMenuStrip";
            this.relayFolderContextMenuStrip.Size = new System.Drawing.Size(163, 48);
            // 
            // expandSubTreeMenuItem12
            // 
            this.expandSubTreeMenuItem12.Name = "expandSubTreeMenuItem12";
            this.expandSubTreeMenuItem12.Size = new System.Drawing.Size(162, 22);
            this.expandSubTreeMenuItem12.Text = "Expand Subtree";
            this.expandSubTreeMenuItem12.ToolTipText = "Expand the subtree.";
            this.expandSubTreeMenuItem12.Click += new System.EventHandler(this.expandEntity_Click);
            // 
            // collapseSubTreeMenuItem12
            // 
            this.collapseSubTreeMenuItem12.Name = "collapseSubTreeMenuItem12";
            this.collapseSubTreeMenuItem12.Size = new System.Drawing.Size(162, 22);
            this.collapseSubTreeMenuItem12.Text = "Collapse Subtree";
            this.collapseSubTreeMenuItem12.ToolTipText = "Collapse the subtree.";
            this.collapseSubTreeMenuItem12.Click += new System.EventHandler(this.collapseEntity_Click);
            // 
            // panelTreeView
            // 
            this.panelTreeView.AutoScroll = true;
            this.panelTreeView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelTreeView.Controls.Add(this.serviceBusTreeView);
            this.panelTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTreeView.ForeColor = System.Drawing.SystemColors.Window;
            this.panelTreeView.HeaderColor1 = System.Drawing.SystemColors.InactiveCaption;
            this.panelTreeView.HeaderColor2 = System.Drawing.SystemColors.ActiveCaption;
            this.panelTreeView.HeaderFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.panelTreeView.HeaderHeight = 24;
            this.panelTreeView.HeaderText = "Service Bus Namespace";
            this.panelTreeView.Icon = ((System.Drawing.Image)(resources.GetObject("panelTreeView.Icon")));
            this.panelTreeView.IconTransparentColor = System.Drawing.Color.White;
            this.panelTreeView.Location = new System.Drawing.Point(0, 0);
            this.panelTreeView.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panelTreeView.Name = "panelTreeView";
            this.panelTreeView.Padding = new System.Windows.Forms.Padding(5, 29, 5, 4);
            this.panelTreeView.Size = new System.Drawing.Size(392, 370);
            this.panelTreeView.TabIndex = 0;
            // 
            // serviceBusTreeView
            // 
            this.serviceBusTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.serviceBusTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serviceBusTreeView.ImageIndex = 0;
            this.serviceBusTreeView.ImageList = this.imageList;
            this.serviceBusTreeView.Indent = 20;
            this.serviceBusTreeView.ItemHeight = 20;
            this.serviceBusTreeView.Location = new System.Drawing.Point(5, 29);
            this.serviceBusTreeView.Name = "serviceBusTreeView";
            this.serviceBusTreeView.SelectedImageIndex = 0;
            this.serviceBusTreeView.Size = new System.Drawing.Size(382, 337);
            this.serviceBusTreeView.TabIndex = 13;
            this.serviceBusTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.serviceBusTreeView_NodeMouseClick);
            // 
            // panelMain
            // 
            this.panelMain.AutoScroll = true;
            this.panelMain.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.ForeColor = System.Drawing.SystemColors.Window;
            this.panelMain.HeaderColor1 = System.Drawing.SystemColors.InactiveCaption;
            this.panelMain.HeaderColor2 = System.Drawing.SystemColors.ActiveCaption;
            this.panelMain.HeaderFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.panelMain.HeaderHeight = 24;
            this.panelMain.HeaderText = "Entity";
            this.panelMain.Icon = ((System.Drawing.Image)(resources.GetObject("panelMain.Icon")));
            this.panelMain.IconTransparentColor = System.Drawing.Color.White;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panelMain.Name = "panelMain";
            this.panelMain.Padding = new System.Windows.Forms.Padding(5, 29, 5, 4);
            this.panelMain.Size = new System.Drawing.Size(820, 370);
            this.panelMain.TabIndex = 0;
            // 
            // panelLog
            // 
            this.panelLog.AutoScroll = true;
            this.panelLog.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelLog.Controls.Add(this.lstLog);
            this.panelLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLog.ForeColor = System.Drawing.SystemColors.Window;
            this.panelLog.HeaderColor1 = System.Drawing.SystemColors.InactiveCaption;
            this.panelLog.HeaderColor2 = System.Drawing.SystemColors.ActiveCaption;
            this.panelLog.HeaderFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.panelLog.HeaderHeight = 24;
            this.panelLog.HeaderText = "Log";
            this.panelLog.Icon = ((System.Drawing.Image)(resources.GetObject("panelLog.Icon")));
            this.panelLog.IconTransparentColor = System.Drawing.Color.White;
            this.panelLog.Location = new System.Drawing.Point(0, 0);
            this.panelLog.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panelLog.Name = "panelLog";
            this.panelLog.Padding = new System.Windows.Forms.Padding(5, 29, 5, 4);
            this.panelLog.Size = new System.Drawing.Size(1216, 370);
            this.panelLog.TabIndex = 0;
            // 
            // lstLog
            // 
            this.lstLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstLog.FormattingEnabled = true;
            this.lstLog.HorizontalScrollbar = true;
            this.lstLog.ItemHeight = 14;
            this.lstLog.Location = new System.Drawing.Point(5, 29);
            this.lstLog.Name = "lstLog";
            this.lstLog.Size = new System.Drawing.Size(1206, 337);
            this.lstLog.TabIndex = 4;
            this.lstLog.Leave += new System.EventHandler(this.lstLog_Leave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1248, 802);
            this.Controls.Add(this.logoPictureBox);
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service Bus Explorer";
            this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.rootContextMenuStrip.ResumeLayout(false);
            this.queuesContextMenuStrip.ResumeLayout(false);
            this.ruleContextMenuStrip.ResumeLayout(false);
            this.rulesContextMenuStrip.ResumeLayout(false);
            this.subscriptionsContextMenuStrip.ResumeLayout(false);
            this.subscriptionContextMenuStrip.ResumeLayout(false);
            this.topicContextMenuStrip.ResumeLayout(false);
            this.queueContextMenuStrip.ResumeLayout(false);
            this.topicsContextMenuStrip.ResumeLayout(false);
            this.relayServicesContextMenuStrip.ResumeLayout(false);
            this.relayServiceContextMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.queueFolderContextMenuStrip.ResumeLayout(false);
            this.topicFolderContextMenuStrip.ResumeLayout(false);
            this.relayFolderContextMenuStrip.ResumeLayout(false);
            this.panelTreeView.ResumeLayout(false);
            this.panelLog.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logWindowToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparatorMain;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private HeaderPanel panelTreeView;
        private HeaderPanel panelMain;
        private HeaderPanel panelLog;
        private System.Windows.Forms.ListBox lstLog;
        private System.Windows.Forms.ToolStripMenuItem saveLogToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.TreeView serviceBusTreeView;
        private System.Windows.Forms.ContextMenuStrip rootContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem refreshRootMenuItem;
        private System.Windows.Forms.ContextMenuStrip queuesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem createQueueMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteQueuesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshQueuesMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem exportQueuesMenuItem;
        private System.Windows.Forms.ContextMenuStrip ruleContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem removeRuleMenuItem;
        private System.Windows.Forms.ContextMenuStrip rulesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addRuleMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem deleteRulesMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem refreshRulesMenuItem;
        private System.Windows.Forms.ContextMenuStrip subscriptionsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addSubscriptionMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem deleteSubscriptionsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshSubscriptionsMenuItem;
        private System.Windows.Forms.ContextMenuStrip subscriptionContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem removeSubscriptionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testSubscriptionMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem addRuleMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem refreshSubscriptionMenuItem;
        private System.Windows.Forms.ContextMenuStrip topicContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteTopicMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testTopicMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem exportTopicMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem addSubscriptionMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteTopicSubscriptionsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshTopicMenuItem;
        private System.Windows.Forms.ContextMenuStrip queueContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteQueueMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testQueueMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshQueueMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem exportQueueMenuItem;
        private System.Windows.Forms.ContextMenuStrip topicsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem createTopicMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTopicsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshTopicsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem exportTopicsMenuItem;
        private System.Windows.Forms.ContextMenuStrip relayServicesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem refreshRelayServicesMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem copyQueueUrlMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyQueueDeadletterQueueUrlMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem copyTopicUrlMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copySubscriptionUrlMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copySubscriptionDeadletterSubscriptionUrlMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem importEntityMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportEntityMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteEntityMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip relayServiceContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyRelayServiceUrlMenuItem;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.PictureBox logoPictureBox;
        private System.Windows.Forms.ContextMenuStrip queueFolderContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem folderCreateQueueMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderDeleteQueuesMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem folderExportQueuesMenuItem;
        private System.Windows.Forms.ContextMenuStrip topicFolderContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem folderCreateTopicMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderDeleteTopicsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem folderExportTopicsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem11;
        private System.Windows.Forms.ContextMenuStrip relayFolderContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem expandSubTreeMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem collapseSubTreeMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem setDefaultLayouToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
    }
}

