﻿#region Copyright
//=======================================================================================
//Microsoft Windows Server AppFabric Customer Advisory Team (CAT)  
//
// This sample is supplemental to the technical guidance published on the community
// blog at http://www.appfabriccat.com/. 
// 
// Author: Paolo Salvatori
//=======================================================================================
// Copyright © 2011 Microsoft Corporation. All rights reserved.
// 
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER 
// EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. YOU BEAR THE RISK OF USING IT.
//=======================================================================================
#endregion

#region Using Directives
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using Microsoft.ServiceBus.Messaging;
#endregion

namespace Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer
{
    public partial class HandleRuleControl : UserControl
    {
        #region Private Constants
        //***************************
        // Formats
        //***************************
        private const string ExceptionFormat = "Exception: {0}";
        private const string InnerExceptionFormat = "InnerException: {0}";

        //***************************
        // Texts
        //***************************
        private const string RemoveText = "Remove";
        private const string AddText = "Add";
        private const string RuleEntity = "RuleDescription";

        //***************************
        // Messages
        //***************************
        private const string NameCannotBeNull = "The Name field cannot be null.";

        //***************************
        // Tooltips
        //***************************
        private const string NameTooltip = "Gets or sets the rule name.";
        private const string FilterExpressionTooltip = "Gets or sets the filter expression.";
        private const string FilterActionTooltip = "Gets or sets the filter action.";
        #endregion

        #region Private Fields
        private readonly RuleWrapper ruleWrapper;
        private readonly ServiceBusHelper serviceBusHelper;
        private readonly MainForm mainForm;
        private bool? isFirstRule = false;
        #endregion

        #region Public Constructors
        public HandleRuleControl(MainForm mainForm, ServiceBusHelper serviceBusHelper, RuleWrapper ruleWrapper, bool? isFirstRule)
        {
            this.mainForm = mainForm;
            this.serviceBusHelper = serviceBusHelper;
            this.ruleWrapper = ruleWrapper;
            this.isFirstRule = isFirstRule;
            InitializeComponent();
            InitializeData();
        } 
        #endregion

        #region Public Events
        public event Action OnCancel;
        #endregion

        #region Private Methods
        private void InitializeData()
        {
            if (ruleWrapper != null &&
                ruleWrapper.SubscriptionDescription != null &&
                ruleWrapper.RuleDescription != null)
            {
                btnAction.Text = RemoveText;
                btnCancel.Enabled = false;
                checkBoxDefault.Checked = true;
                checkBoxDefault.Enabled = false;
                checkBoxDefault.CheckedChanged += checkBoxDefault_CheckedChanged;
                SetReadOnly(this);
                if (!string.IsNullOrEmpty(ruleWrapper.RuleDescription.Name))
                {
                    txtName.Text = ruleWrapper.RuleDescription.Name;
                }
                if (ruleWrapper.RuleDescription.Filter != null &&
                    ruleWrapper.RuleDescription.Filter is SqlFilter)
                {
                    txtSqlFilterExpression.Text = (ruleWrapper.RuleDescription.Filter as SqlFilter).SqlExpression ?? string.Empty;
                }
                if (ruleWrapper.RuleDescription.Action != null &&
                    ruleWrapper.RuleDescription.Action is SqlRuleAction)
                {
                    txtSqlFilterAction.Text = (ruleWrapper.RuleDescription.Action as SqlRuleAction).SqlExpression ?? string.Empty;
                }
                toolTip.SetToolTip(txtName, NameTooltip);
                toolTip.SetToolTip(txtSqlFilterExpression, FilterExpressionTooltip);
                toolTip.SetToolTip(txtSqlFilterAction, FilterActionTooltip);
            }
            else
            {
                btnAction.Text = AddText;
                if (isFirstRule.HasValue)
                {
                    checkBoxDefault.Checked = isFirstRule.Value;
                }
            }
            txtName.Focus();
        }

        private void SetReadOnly(Control control)
        {
            if (control != null &&
                control.Controls.Count > 0)
            {
                for (int i = 0; i < control.Controls.Count; i++)
                {
                    if (control.Controls[i] is TextBox)
                    {
                        var textBox = ((TextBox)(control.Controls[i]));
                        textBox.ReadOnly = true;
                        textBox.BackColor = SystemColors.Window;
                        continue;
                    }
                    SetReadOnly(control.Controls[i]);
                }
            }
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            try
            {
                if (serviceBusHelper == null ||
                    ruleWrapper == null ||
                    ruleWrapper.SubscriptionDescription == null)
                {
                    return;
                }
                if (btnAction.Text == RemoveText &&
                    ruleWrapper.SubscriptionDescription != null &&
                    !string.IsNullOrEmpty(ruleWrapper.SubscriptionDescription.Name) &&
                    ruleWrapper.RuleDescription != null &&
                    !string.IsNullOrEmpty(ruleWrapper.RuleDescription.Name))
                {
                    var deleteForm = new DeleteForm(ruleWrapper.RuleDescription.Name, RuleEntity.ToLower());
                    if (deleteForm.ShowDialog() == DialogResult.OK)
                    {
                        serviceBusHelper.RemoveRule(ruleWrapper.SubscriptionDescription, ruleWrapper.RuleDescription);
                    }
                    return;
                }
                if (btnAction.Text == AddText)
                {
                    if (string.IsNullOrEmpty(txtName.Text))
                    {
                        mainForm.WriteToLog(NameCannotBeNull);
                        return;
                    }

                    var ruleDescription = new RuleDescription(txtName.Text);

                    if (!string.IsNullOrEmpty(txtSqlFilterExpression.Text))
                    {
                        ruleDescription.Filter = new SqlFilter(txtSqlFilterExpression.Text);
                    }
                    if (!string.IsNullOrEmpty(txtSqlFilterAction.Text))
                    {
                        ruleDescription.Action = new SqlRuleAction(txtSqlFilterAction.Text);
                    }

                    ruleWrapper.RuleDescription = serviceBusHelper.AddRule(ruleWrapper.SubscriptionDescription, ruleDescription);
                    InitializeData();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void HandleException(Exception ex)
        {
            if (ex != null && !string.IsNullOrEmpty(ex.Message))
            {
                mainForm.WriteToLog(string.Format(CultureInfo.CurrentCulture, ExceptionFormat, ex.Message));
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    mainForm.WriteToLog(string.Format(CultureInfo.CurrentCulture, InnerExceptionFormat, ex.InnerException.Message));
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            OnCancel();
        }

        private void checkBoxDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxDefault.Checked)
            {
                txtName.Text = RuleDescription.DefaultRuleName;
            }
        }
        #endregion
    }
}
