﻿namespace Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer
{
    partial class HandleSubscriptionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxName = new System.Windows.Forms.GroupBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.groupBoxDefaultMessageTimeToLive = new System.Windows.Forms.GroupBox();
            this.lblDefaultMessageTimeToLiveMilliseconds = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveMilliseconds = new System.Windows.Forms.TextBox();
            this.lblDefaultMessageTimeToLiveSeconds = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveSeconds = new System.Windows.Forms.TextBox();
            this.lblDefaultMessageTimeToLiveMinutes = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveMinutes = new System.Windows.Forms.TextBox();
            this.lbllblDefaultMessageTimeToLiveHours = new System.Windows.Forms.Label();
            this.lblDefaultMessageTimeToLiveDays = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveHours = new System.Windows.Forms.TextBox();
            this.txtDefaultMessageTimeToLiveDays = new System.Windows.Forms.TextBox();
            this.groupBoxFlags = new System.Windows.Forms.GroupBox();
            this.checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.btnAction = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBoxDefaultRule = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFilter = new System.Windows.Forms.Label();
            this.txtAction = new System.Windows.Forms.TextBox();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxLockDuration = new System.Windows.Forms.GroupBox();
            this.lblLockDurationMilliseconds = new System.Windows.Forms.Label();
            this.txtLockDurationMilliseconds = new System.Windows.Forms.TextBox();
            this.lblLockDurationSeconds = new System.Windows.Forms.Label();
            this.txtLockDurationSeconds = new System.Windows.Forms.TextBox();
            this.lblLockDurationMinutes = new System.Windows.Forms.Label();
            this.txtLockDurationMinutes = new System.Windows.Forms.TextBox();
            this.lblLockDurationHours = new System.Windows.Forms.Label();
            this.lblLockDurationDays = new System.Windows.Forms.Label();
            this.txtLockDurationHours = new System.Windows.Forms.TextBox();
            this.txtLockDurationDays = new System.Windows.Forms.TextBox();
            this.groupBoxMessageCount = new System.Windows.Forms.GroupBox();
            this.txtMessageCount = new System.Windows.Forms.TextBox();
            this.groupBoxMaxDeliveryCount = new System.Windows.Forms.GroupBox();
            this.txtMaxDeliveryCount = new System.Windows.Forms.TextBox();
            this.groupBoxName.SuspendLayout();
            this.groupBoxDefaultMessageTimeToLive.SuspendLayout();
            this.groupBoxFlags.SuspendLayout();
            this.groupBoxDefaultRule.SuspendLayout();
            this.groupBoxLockDuration.SuspendLayout();
            this.groupBoxMessageCount.SuspendLayout();
            this.groupBoxMaxDeliveryCount.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxName
            // 
            this.groupBoxName.Controls.Add(this.txtName);
            this.groupBoxName.Location = new System.Drawing.Point(16, 8);
            this.groupBoxName.Name = "groupBoxName";
            this.groupBoxName.Size = new System.Drawing.Size(384, 72);
            this.groupBoxName.TabIndex = 4;
            this.groupBoxName.TabStop = false;
            this.groupBoxName.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(16, 40);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(352, 20);
            this.txtName.TabIndex = 0;
            // 
            // groupBoxDefaultMessageTimeToLive
            // 
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveMilliseconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveMilliseconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveSeconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveSeconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveMinutes);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveMinutes);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lbllblDefaultMessageTimeToLiveHours);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveDays);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveHours);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveDays);
            this.groupBoxDefaultMessageTimeToLive.Location = new System.Drawing.Point(16, 96);
            this.groupBoxDefaultMessageTimeToLive.Name = "groupBoxDefaultMessageTimeToLive";
            this.groupBoxDefaultMessageTimeToLive.Size = new System.Drawing.Size(384, 72);
            this.groupBoxDefaultMessageTimeToLive.TabIndex = 5;
            this.groupBoxDefaultMessageTimeToLive.TabStop = false;
            this.groupBoxDefaultMessageTimeToLive.Text = "Default Message Time To Live";
            // 
            // lblDefaultMessageTimeToLiveMilliseconds
            // 
            this.lblDefaultMessageTimeToLiveMilliseconds.AutoSize = true;
            this.lblDefaultMessageTimeToLiveMilliseconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveMilliseconds.Location = new System.Drawing.Point(304, 24);
            this.lblDefaultMessageTimeToLiveMilliseconds.Name = "lblDefaultMessageTimeToLiveMilliseconds";
            this.lblDefaultMessageTimeToLiveMilliseconds.Size = new System.Drawing.Size(67, 13);
            this.lblDefaultMessageTimeToLiveMilliseconds.TabIndex = 15;
            this.lblDefaultMessageTimeToLiveMilliseconds.Text = "Milliseconds:";
            // 
            // txtDefaultMessageTimeToLiveMilliseconds
            // 
            this.txtDefaultMessageTimeToLiveMilliseconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveMilliseconds.Location = new System.Drawing.Point(304, 40);
            this.txtDefaultMessageTimeToLiveMilliseconds.Name = "txtDefaultMessageTimeToLiveMilliseconds";
            this.txtDefaultMessageTimeToLiveMilliseconds.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveMilliseconds.TabIndex = 4;
            // 
            // lblDefaultMessageTimeToLiveSeconds
            // 
            this.lblDefaultMessageTimeToLiveSeconds.AutoSize = true;
            this.lblDefaultMessageTimeToLiveSeconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveSeconds.Location = new System.Drawing.Point(232, 24);
            this.lblDefaultMessageTimeToLiveSeconds.Name = "lblDefaultMessageTimeToLiveSeconds";
            this.lblDefaultMessageTimeToLiveSeconds.Size = new System.Drawing.Size(52, 13);
            this.lblDefaultMessageTimeToLiveSeconds.TabIndex = 13;
            this.lblDefaultMessageTimeToLiveSeconds.Text = "Seconds:";
            // 
            // txtDefaultMessageTimeToLiveSeconds
            // 
            this.txtDefaultMessageTimeToLiveSeconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveSeconds.Location = new System.Drawing.Point(232, 40);
            this.txtDefaultMessageTimeToLiveSeconds.Name = "txtDefaultMessageTimeToLiveSeconds";
            this.txtDefaultMessageTimeToLiveSeconds.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveSeconds.TabIndex = 3;
            // 
            // lblDefaultMessageTimeToLiveMinutes
            // 
            this.lblDefaultMessageTimeToLiveMinutes.AutoSize = true;
            this.lblDefaultMessageTimeToLiveMinutes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveMinutes.Location = new System.Drawing.Point(160, 24);
            this.lblDefaultMessageTimeToLiveMinutes.Name = "lblDefaultMessageTimeToLiveMinutes";
            this.lblDefaultMessageTimeToLiveMinutes.Size = new System.Drawing.Size(47, 13);
            this.lblDefaultMessageTimeToLiveMinutes.TabIndex = 11;
            this.lblDefaultMessageTimeToLiveMinutes.Text = "Minutes:";
            // 
            // txtDefaultMessageTimeToLiveMinutes
            // 
            this.txtDefaultMessageTimeToLiveMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveMinutes.Location = new System.Drawing.Point(160, 40);
            this.txtDefaultMessageTimeToLiveMinutes.Name = "txtDefaultMessageTimeToLiveMinutes";
            this.txtDefaultMessageTimeToLiveMinutes.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveMinutes.TabIndex = 2;
            // 
            // lbllblDefaultMessageTimeToLiveHours
            // 
            this.lbllblDefaultMessageTimeToLiveHours.AutoSize = true;
            this.lbllblDefaultMessageTimeToLiveHours.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbllblDefaultMessageTimeToLiveHours.Location = new System.Drawing.Point(88, 24);
            this.lbllblDefaultMessageTimeToLiveHours.Name = "lbllblDefaultMessageTimeToLiveHours";
            this.lbllblDefaultMessageTimeToLiveHours.Size = new System.Drawing.Size(38, 13);
            this.lbllblDefaultMessageTimeToLiveHours.TabIndex = 9;
            this.lbllblDefaultMessageTimeToLiveHours.Text = "Hours:";
            // 
            // lblDefaultMessageTimeToLiveDays
            // 
            this.lblDefaultMessageTimeToLiveDays.AutoSize = true;
            this.lblDefaultMessageTimeToLiveDays.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveDays.Location = new System.Drawing.Point(16, 24);
            this.lblDefaultMessageTimeToLiveDays.Name = "lblDefaultMessageTimeToLiveDays";
            this.lblDefaultMessageTimeToLiveDays.Size = new System.Drawing.Size(34, 13);
            this.lblDefaultMessageTimeToLiveDays.TabIndex = 7;
            this.lblDefaultMessageTimeToLiveDays.Text = "Days:";
            // 
            // txtDefaultMessageTimeToLiveHours
            // 
            this.txtDefaultMessageTimeToLiveHours.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveHours.Location = new System.Drawing.Point(88, 40);
            this.txtDefaultMessageTimeToLiveHours.Name = "txtDefaultMessageTimeToLiveHours";
            this.txtDefaultMessageTimeToLiveHours.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveHours.TabIndex = 1;
            // 
            // txtDefaultMessageTimeToLiveDays
            // 
            this.txtDefaultMessageTimeToLiveDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveDays.Location = new System.Drawing.Point(16, 40);
            this.txtDefaultMessageTimeToLiveDays.Name = "txtDefaultMessageTimeToLiveDays";
            this.txtDefaultMessageTimeToLiveDays.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveDays.TabIndex = 0;
            // 
            // groupBoxFlags
            // 
            this.groupBoxFlags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFlags.Controls.Add(this.checkedListBox);
            this.groupBoxFlags.Location = new System.Drawing.Point(416, 184);
            this.groupBoxFlags.Name = "groupBoxFlags";
            this.groupBoxFlags.Size = new System.Drawing.Size(384, 112);
            this.groupBoxFlags.TabIndex = 7;
            this.groupBoxFlags.TabStop = false;
            this.groupBoxFlags.Text = "Subscription Settings";
            // 
            // checkedListBox
            // 
            this.checkedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox.CheckOnClick = true;
            this.checkedListBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkedListBox.FormattingEnabled = true;
            this.checkedListBox.Items.AddRange(new object[] {
            "Enable Batched Operations ",
            "Enable Dead Lettering On Filter Evaluation Exceptions",
            "Enable Dead Lettering On MessageExpiration",
            "Requires Session"});
            this.checkedListBox.Location = new System.Drawing.Point(16, 28);
            this.checkedListBox.Margin = new System.Windows.Forms.Padding(8);
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(352, 64);
            this.checkedListBox.TabIndex = 0;
            this.checkedListBox.ThreeDCheckBoxes = true;
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnAction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAction.Location = new System.Drawing.Point(648, 304);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(72, 24);
            this.btnAction.TabIndex = 0;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = false;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Location = new System.Drawing.Point(728, 304);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 24);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBoxDefaultRule
            // 
            this.groupBoxDefaultRule.Controls.Add(this.label1);
            this.groupBoxDefaultRule.Controls.Add(this.lblFilter);
            this.groupBoxDefaultRule.Controls.Add(this.txtAction);
            this.groupBoxDefaultRule.Controls.Add(this.txtFilter);
            this.groupBoxDefaultRule.Location = new System.Drawing.Point(16, 184);
            this.groupBoxDefaultRule.Name = "groupBoxDefaultRule";
            this.groupBoxDefaultRule.Size = new System.Drawing.Size(384, 112);
            this.groupBoxDefaultRule.TabIndex = 14;
            this.groupBoxDefaultRule.TabStop = false;
            this.groupBoxDefaultRule.Text = "Default Rule";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(16, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Action:";
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblFilter.Location = new System.Drawing.Point(16, 24);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(32, 13);
            this.lblFilter.TabIndex = 21;
            this.lblFilter.Text = "Filter:";
            // 
            // txtAction
            // 
            this.txtAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAction.BackColor = System.Drawing.SystemColors.Window;
            this.txtAction.Location = new System.Drawing.Point(16, 80);
            this.txtAction.Name = "txtAction";
            this.txtAction.Size = new System.Drawing.Size(352, 20);
            this.txtAction.TabIndex = 1;
            // 
            // txtFilter
            // 
            this.txtFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilter.BackColor = System.Drawing.SystemColors.Window;
            this.txtFilter.Location = new System.Drawing.Point(16, 40);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(352, 20);
            this.txtFilter.TabIndex = 0;
            this.txtFilter.Text = "1=1";
            // 
            // groupBoxLockDuration
            // 
            this.groupBoxLockDuration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationMilliseconds);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationMilliseconds);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationSeconds);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationSeconds);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationMinutes);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationMinutes);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationHours);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationDays);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationHours);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationDays);
            this.groupBoxLockDuration.Location = new System.Drawing.Point(416, 96);
            this.groupBoxLockDuration.Name = "groupBoxLockDuration";
            this.groupBoxLockDuration.Size = new System.Drawing.Size(384, 72);
            this.groupBoxLockDuration.TabIndex = 16;
            this.groupBoxLockDuration.TabStop = false;
            this.groupBoxLockDuration.Text = "Lock Duration";
            // 
            // lblLockDurationMilliseconds
            // 
            this.lblLockDurationMilliseconds.AutoSize = true;
            this.lblLockDurationMilliseconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationMilliseconds.Location = new System.Drawing.Point(304, 24);
            this.lblLockDurationMilliseconds.Name = "lblLockDurationMilliseconds";
            this.lblLockDurationMilliseconds.Size = new System.Drawing.Size(67, 13);
            this.lblLockDurationMilliseconds.TabIndex = 15;
            this.lblLockDurationMilliseconds.Text = "Milliseconds:";
            // 
            // txtLockDurationMilliseconds
            // 
            this.txtLockDurationMilliseconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationMilliseconds.Location = new System.Drawing.Point(304, 40);
            this.txtLockDurationMilliseconds.Name = "txtLockDurationMilliseconds";
            this.txtLockDurationMilliseconds.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationMilliseconds.TabIndex = 4;
            // 
            // lblLockDurationSeconds
            // 
            this.lblLockDurationSeconds.AutoSize = true;
            this.lblLockDurationSeconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationSeconds.Location = new System.Drawing.Point(232, 24);
            this.lblLockDurationSeconds.Name = "lblLockDurationSeconds";
            this.lblLockDurationSeconds.Size = new System.Drawing.Size(52, 13);
            this.lblLockDurationSeconds.TabIndex = 13;
            this.lblLockDurationSeconds.Text = "Seconds:";
            // 
            // txtLockDurationSeconds
            // 
            this.txtLockDurationSeconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationSeconds.Location = new System.Drawing.Point(232, 40);
            this.txtLockDurationSeconds.Name = "txtLockDurationSeconds";
            this.txtLockDurationSeconds.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationSeconds.TabIndex = 3;
            // 
            // lblLockDurationMinutes
            // 
            this.lblLockDurationMinutes.AutoSize = true;
            this.lblLockDurationMinutes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationMinutes.Location = new System.Drawing.Point(160, 24);
            this.lblLockDurationMinutes.Name = "lblLockDurationMinutes";
            this.lblLockDurationMinutes.Size = new System.Drawing.Size(47, 13);
            this.lblLockDurationMinutes.TabIndex = 11;
            this.lblLockDurationMinutes.Text = "Minutes:";
            // 
            // txtLockDurationMinutes
            // 
            this.txtLockDurationMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationMinutes.Location = new System.Drawing.Point(160, 40);
            this.txtLockDurationMinutes.Name = "txtLockDurationMinutes";
            this.txtLockDurationMinutes.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationMinutes.TabIndex = 2;
            // 
            // lblLockDurationHours
            // 
            this.lblLockDurationHours.AutoSize = true;
            this.lblLockDurationHours.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationHours.Location = new System.Drawing.Point(88, 24);
            this.lblLockDurationHours.Name = "lblLockDurationHours";
            this.lblLockDurationHours.Size = new System.Drawing.Size(38, 13);
            this.lblLockDurationHours.TabIndex = 9;
            this.lblLockDurationHours.Text = "Hours:";
            // 
            // lblLockDurationDays
            // 
            this.lblLockDurationDays.AutoSize = true;
            this.lblLockDurationDays.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationDays.Location = new System.Drawing.Point(16, 24);
            this.lblLockDurationDays.Name = "lblLockDurationDays";
            this.lblLockDurationDays.Size = new System.Drawing.Size(34, 13);
            this.lblLockDurationDays.TabIndex = 7;
            this.lblLockDurationDays.Text = "Days:";
            // 
            // txtLockDurationHours
            // 
            this.txtLockDurationHours.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationHours.Location = new System.Drawing.Point(88, 40);
            this.txtLockDurationHours.Name = "txtLockDurationHours";
            this.txtLockDurationHours.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationHours.TabIndex = 1;
            // 
            // txtLockDurationDays
            // 
            this.txtLockDurationDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationDays.Location = new System.Drawing.Point(16, 40);
            this.txtLockDurationDays.Name = "txtLockDurationDays";
            this.txtLockDurationDays.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationDays.TabIndex = 0;
            // 
            // groupBoxMessageCount
            // 
            this.groupBoxMessageCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMessageCount.Controls.Add(this.txtMessageCount);
            this.groupBoxMessageCount.Location = new System.Drawing.Point(608, 8);
            this.groupBoxMessageCount.Name = "groupBoxMessageCount";
            this.groupBoxMessageCount.Size = new System.Drawing.Size(184, 72);
            this.groupBoxMessageCount.TabIndex = 19;
            this.groupBoxMessageCount.TabStop = false;
            this.groupBoxMessageCount.Text = "Message Count";
            // 
            // txtMessageCount
            // 
            this.txtMessageCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMessageCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMessageCount.Location = new System.Drawing.Point(16, 40);
            this.txtMessageCount.Name = "txtMessageCount";
            this.txtMessageCount.ReadOnly = true;
            this.txtMessageCount.Size = new System.Drawing.Size(152, 20);
            this.txtMessageCount.TabIndex = 20;
            // 
            // groupBoxMaxDeliveryCount
            // 
            this.groupBoxMaxDeliveryCount.Controls.Add(this.txtMaxDeliveryCount);
            this.groupBoxMaxDeliveryCount.Location = new System.Drawing.Point(416, 8);
            this.groupBoxMaxDeliveryCount.Name = "groupBoxMaxDeliveryCount";
            this.groupBoxMaxDeliveryCount.Size = new System.Drawing.Size(184, 72);
            this.groupBoxMaxDeliveryCount.TabIndex = 20;
            this.groupBoxMaxDeliveryCount.TabStop = false;
            this.groupBoxMaxDeliveryCount.Text = "Max Delivery Count";
            // 
            // txtMaxDeliveryCount
            // 
            this.txtMaxDeliveryCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxDeliveryCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMaxDeliveryCount.Location = new System.Drawing.Point(16, 40);
            this.txtMaxDeliveryCount.Name = "txtMaxDeliveryCount";
            this.txtMaxDeliveryCount.Size = new System.Drawing.Size(152, 20);
            this.txtMaxDeliveryCount.TabIndex = 18;
            // 
            // HandleSubscriptionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.groupBoxMaxDeliveryCount);
            this.Controls.Add(this.groupBoxMessageCount);
            this.Controls.Add(this.groupBoxLockDuration);
            this.Controls.Add(this.groupBoxDefaultRule);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.groupBoxFlags);
            this.Controls.Add(this.groupBoxDefaultMessageTimeToLive);
            this.Controls.Add(this.groupBoxName);
            this.Name = "HandleSubscriptionControl";
            this.Size = new System.Drawing.Size(816, 344);
            this.groupBoxName.ResumeLayout(false);
            this.groupBoxName.PerformLayout();
            this.groupBoxDefaultMessageTimeToLive.ResumeLayout(false);
            this.groupBoxDefaultMessageTimeToLive.PerformLayout();
            this.groupBoxFlags.ResumeLayout(false);
            this.groupBoxDefaultRule.ResumeLayout(false);
            this.groupBoxDefaultRule.PerformLayout();
            this.groupBoxLockDuration.ResumeLayout(false);
            this.groupBoxLockDuration.PerformLayout();
            this.groupBoxMessageCount.ResumeLayout(false);
            this.groupBoxMessageCount.PerformLayout();
            this.groupBoxMaxDeliveryCount.ResumeLayout(false);
            this.groupBoxMaxDeliveryCount.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxName;
        private System.Windows.Forms.GroupBox groupBoxDefaultMessageTimeToLive;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveMilliseconds;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveMilliseconds;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveSeconds;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveSeconds;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveMinutes;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveMinutes;
        private System.Windows.Forms.Label lbllblDefaultMessageTimeToLiveHours;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveDays;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveHours;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveDays;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.GroupBox groupBoxFlags;
        private System.Windows.Forms.CheckedListBox checkedListBox;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBoxDefaultRule;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.GroupBox groupBoxLockDuration;
        private System.Windows.Forms.Label lblLockDurationMilliseconds;
        private System.Windows.Forms.TextBox txtLockDurationMilliseconds;
        private System.Windows.Forms.Label lblLockDurationSeconds;
        private System.Windows.Forms.TextBox txtLockDurationSeconds;
        private System.Windows.Forms.Label lblLockDurationMinutes;
        private System.Windows.Forms.TextBox txtLockDurationMinutes;
        private System.Windows.Forms.Label lblLockDurationHours;
        private System.Windows.Forms.Label lblLockDurationDays;
        private System.Windows.Forms.TextBox txtLockDurationHours;
        private System.Windows.Forms.TextBox txtLockDurationDays;
        private System.Windows.Forms.GroupBox groupBoxMessageCount;
        private System.Windows.Forms.TextBox txtMessageCount;
        private System.Windows.Forms.GroupBox groupBoxMaxDeliveryCount;
        private System.Windows.Forms.TextBox txtMaxDeliveryCount;
        private System.Windows.Forms.TextBox txtAction;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFilter;
    }
}
