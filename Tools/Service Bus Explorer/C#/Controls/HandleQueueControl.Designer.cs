﻿namespace Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer
{
    partial class HandleQueueControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxPath = new System.Windows.Forms.GroupBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.groupBoxDefaultMessageTimeToLive = new System.Windows.Forms.GroupBox();
            this.lblDefaultMessageTimeToLiveMilliseconds = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveMilliseconds = new System.Windows.Forms.TextBox();
            this.lblDefaultMessageTimeToLiveSeconds = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveSeconds = new System.Windows.Forms.TextBox();
            this.lblDefaultMessageTimeToLiveMinutes = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveMinutes = new System.Windows.Forms.TextBox();
            this.lbllblDefaultMessageTimeToLiveHours = new System.Windows.Forms.Label();
            this.lblDefaultMessageTimeToLiveDays = new System.Windows.Forms.Label();
            this.txtDefaultMessageTimeToLiveHours = new System.Windows.Forms.TextBox();
            this.txtDefaultMessageTimeToLiveDays = new System.Windows.Forms.TextBox();
            this.groupBoxDuplicateDetectionHistoryTimeWindow = new System.Windows.Forms.GroupBox();
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds = new System.Windows.Forms.Label();
            this.txtDuplicateDetectionHistoryTimeWindowMilliseconds = new System.Windows.Forms.TextBox();
            this.lblDuplicateDetectionHistoryTimeWindowSeconds = new System.Windows.Forms.Label();
            this.txtDuplicateDetectionHistoryTimeWindowSeconds = new System.Windows.Forms.TextBox();
            this.lblDuplicateDetectionHistoryTimeWindowMinutes = new System.Windows.Forms.Label();
            this.txtDuplicateDetectionHistoryTimeWindowMinutes = new System.Windows.Forms.TextBox();
            this.lblDuplicateDetectionHistoryTimeWindowHours = new System.Windows.Forms.Label();
            this.lblDuplicateDetectionHistoryTimeWindowDays = new System.Windows.Forms.Label();
            this.txtDuplicateDetectionHistoryTimeWindowHours = new System.Windows.Forms.TextBox();
            this.txtDuplicateDetectionHistoryTimeWindowDays = new System.Windows.Forms.TextBox();
            this.groupBoxFlags = new System.Windows.Forms.GroupBox();
            this.checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.groupBoxLockDuration = new System.Windows.Forms.GroupBox();
            this.lblLockDurationMilliseconds = new System.Windows.Forms.Label();
            this.txtLockDurationMilliseconds = new System.Windows.Forms.TextBox();
            this.lblLockDurationSeconds = new System.Windows.Forms.Label();
            this.txtLockDurationSeconds = new System.Windows.Forms.TextBox();
            this.lblLockDurationMinutes = new System.Windows.Forms.Label();
            this.txtLockDurationMinutes = new System.Windows.Forms.TextBox();
            this.lblLockDurationHours = new System.Windows.Forms.Label();
            this.lblLockDurationDays = new System.Windows.Forms.Label();
            this.txtLockDurationHours = new System.Windows.Forms.TextBox();
            this.txtLockDurationDays = new System.Windows.Forms.TextBox();
            this.groupBoxMaxQueueSizeInBytes = new System.Windows.Forms.GroupBox();
            this.lblSizeInBytes = new System.Windows.Forms.Label();
            this.txtSizeInBytes = new System.Windows.Forms.TextBox();
            this.lblMessageCount = new System.Windows.Forms.Label();
            this.lblMaxDeliveryCount = new System.Windows.Forms.Label();
            this.txtMessageCount = new System.Windows.Forms.TextBox();
            this.txtMaxDeliveryCount = new System.Windows.Forms.TextBox();
            this.lblMaxQueueSizeInMegabytes = new System.Windows.Forms.Label();
            this.txtMaxQueueSizeInMegabytes = new System.Windows.Forms.TextBox();
            this.btnAction = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxPath.SuspendLayout();
            this.groupBoxDefaultMessageTimeToLive.SuspendLayout();
            this.groupBoxDuplicateDetectionHistoryTimeWindow.SuspendLayout();
            this.groupBoxFlags.SuspendLayout();
            this.groupBoxLockDuration.SuspendLayout();
            this.groupBoxMaxQueueSizeInBytes.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxPath
            // 
            this.groupBoxPath.Controls.Add(this.txtPath);
            this.groupBoxPath.Location = new System.Drawing.Point(16, 8);
            this.groupBoxPath.Name = "groupBoxPath";
            this.groupBoxPath.Size = new System.Drawing.Size(384, 72);
            this.groupBoxPath.TabIndex = 4;
            this.groupBoxPath.TabStop = false;
            this.groupBoxPath.Text = "Path";
            // 
            // txtPath
            // 
            this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPath.BackColor = System.Drawing.SystemColors.Window;
            this.txtPath.Location = new System.Drawing.Point(16, 40);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(352, 20);
            this.txtPath.TabIndex = 0;
            // 
            // groupBoxDefaultMessageTimeToLive
            // 
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveMilliseconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveMilliseconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveSeconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveSeconds);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveMinutes);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveMinutes);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lbllblDefaultMessageTimeToLiveHours);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.lblDefaultMessageTimeToLiveDays);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveHours);
            this.groupBoxDefaultMessageTimeToLive.Controls.Add(this.txtDefaultMessageTimeToLiveDays);
            this.groupBoxDefaultMessageTimeToLive.Location = new System.Drawing.Point(16, 96);
            this.groupBoxDefaultMessageTimeToLive.Name = "groupBoxDefaultMessageTimeToLive";
            this.groupBoxDefaultMessageTimeToLive.Size = new System.Drawing.Size(384, 72);
            this.groupBoxDefaultMessageTimeToLive.TabIndex = 5;
            this.groupBoxDefaultMessageTimeToLive.TabStop = false;
            this.groupBoxDefaultMessageTimeToLive.Text = "Default Message Time To Live";
            // 
            // lblDefaultMessageTimeToLiveMilliseconds
            // 
            this.lblDefaultMessageTimeToLiveMilliseconds.AutoSize = true;
            this.lblDefaultMessageTimeToLiveMilliseconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveMilliseconds.Location = new System.Drawing.Point(304, 24);
            this.lblDefaultMessageTimeToLiveMilliseconds.Name = "lblDefaultMessageTimeToLiveMilliseconds";
            this.lblDefaultMessageTimeToLiveMilliseconds.Size = new System.Drawing.Size(67, 13);
            this.lblDefaultMessageTimeToLiveMilliseconds.TabIndex = 15;
            this.lblDefaultMessageTimeToLiveMilliseconds.Text = "Milliseconds:";
            // 
            // txtDefaultMessageTimeToLiveMilliseconds
            // 
            this.txtDefaultMessageTimeToLiveMilliseconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveMilliseconds.Location = new System.Drawing.Point(304, 40);
            this.txtDefaultMessageTimeToLiveMilliseconds.Name = "txtDefaultMessageTimeToLiveMilliseconds";
            this.txtDefaultMessageTimeToLiveMilliseconds.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveMilliseconds.TabIndex = 4;
            // 
            // lblDefaultMessageTimeToLiveSeconds
            // 
            this.lblDefaultMessageTimeToLiveSeconds.AutoSize = true;
            this.lblDefaultMessageTimeToLiveSeconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveSeconds.Location = new System.Drawing.Point(232, 24);
            this.lblDefaultMessageTimeToLiveSeconds.Name = "lblDefaultMessageTimeToLiveSeconds";
            this.lblDefaultMessageTimeToLiveSeconds.Size = new System.Drawing.Size(52, 13);
            this.lblDefaultMessageTimeToLiveSeconds.TabIndex = 13;
            this.lblDefaultMessageTimeToLiveSeconds.Text = "Seconds:";
            // 
            // txtDefaultMessageTimeToLiveSeconds
            // 
            this.txtDefaultMessageTimeToLiveSeconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveSeconds.Location = new System.Drawing.Point(232, 40);
            this.txtDefaultMessageTimeToLiveSeconds.Name = "txtDefaultMessageTimeToLiveSeconds";
            this.txtDefaultMessageTimeToLiveSeconds.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveSeconds.TabIndex = 3;
            // 
            // lblDefaultMessageTimeToLiveMinutes
            // 
            this.lblDefaultMessageTimeToLiveMinutes.AutoSize = true;
            this.lblDefaultMessageTimeToLiveMinutes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveMinutes.Location = new System.Drawing.Point(160, 24);
            this.lblDefaultMessageTimeToLiveMinutes.Name = "lblDefaultMessageTimeToLiveMinutes";
            this.lblDefaultMessageTimeToLiveMinutes.Size = new System.Drawing.Size(47, 13);
            this.lblDefaultMessageTimeToLiveMinutes.TabIndex = 11;
            this.lblDefaultMessageTimeToLiveMinutes.Text = "Minutes:";
            // 
            // txtDefaultMessageTimeToLiveMinutes
            // 
            this.txtDefaultMessageTimeToLiveMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveMinutes.Location = new System.Drawing.Point(160, 40);
            this.txtDefaultMessageTimeToLiveMinutes.Name = "txtDefaultMessageTimeToLiveMinutes";
            this.txtDefaultMessageTimeToLiveMinutes.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveMinutes.TabIndex = 2;
            // 
            // lbllblDefaultMessageTimeToLiveHours
            // 
            this.lbllblDefaultMessageTimeToLiveHours.AutoSize = true;
            this.lbllblDefaultMessageTimeToLiveHours.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbllblDefaultMessageTimeToLiveHours.Location = new System.Drawing.Point(88, 24);
            this.lbllblDefaultMessageTimeToLiveHours.Name = "lbllblDefaultMessageTimeToLiveHours";
            this.lbllblDefaultMessageTimeToLiveHours.Size = new System.Drawing.Size(38, 13);
            this.lbllblDefaultMessageTimeToLiveHours.TabIndex = 9;
            this.lbllblDefaultMessageTimeToLiveHours.Text = "Hours:";
            // 
            // lblDefaultMessageTimeToLiveDays
            // 
            this.lblDefaultMessageTimeToLiveDays.AutoSize = true;
            this.lblDefaultMessageTimeToLiveDays.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDefaultMessageTimeToLiveDays.Location = new System.Drawing.Point(16, 24);
            this.lblDefaultMessageTimeToLiveDays.Name = "lblDefaultMessageTimeToLiveDays";
            this.lblDefaultMessageTimeToLiveDays.Size = new System.Drawing.Size(34, 13);
            this.lblDefaultMessageTimeToLiveDays.TabIndex = 7;
            this.lblDefaultMessageTimeToLiveDays.Text = "Days:";
            // 
            // txtDefaultMessageTimeToLiveHours
            // 
            this.txtDefaultMessageTimeToLiveHours.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveHours.Location = new System.Drawing.Point(88, 40);
            this.txtDefaultMessageTimeToLiveHours.Name = "txtDefaultMessageTimeToLiveHours";
            this.txtDefaultMessageTimeToLiveHours.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveHours.TabIndex = 1;
            // 
            // txtDefaultMessageTimeToLiveDays
            // 
            this.txtDefaultMessageTimeToLiveDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultMessageTimeToLiveDays.Location = new System.Drawing.Point(16, 40);
            this.txtDefaultMessageTimeToLiveDays.Name = "txtDefaultMessageTimeToLiveDays";
            this.txtDefaultMessageTimeToLiveDays.Size = new System.Drawing.Size(64, 20);
            this.txtDefaultMessageTimeToLiveDays.TabIndex = 0;
            // 
            // groupBoxDuplicateDetectionHistoryTimeWindow
            // 
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.lblDuplicateDetectionHistoryTimeWindowMilliseconds);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.txtDuplicateDetectionHistoryTimeWindowMilliseconds);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.lblDuplicateDetectionHistoryTimeWindowSeconds);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.txtDuplicateDetectionHistoryTimeWindowSeconds);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.lblDuplicateDetectionHistoryTimeWindowMinutes);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.txtDuplicateDetectionHistoryTimeWindowMinutes);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.lblDuplicateDetectionHistoryTimeWindowHours);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.lblDuplicateDetectionHistoryTimeWindowDays);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.txtDuplicateDetectionHistoryTimeWindowHours);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Controls.Add(this.txtDuplicateDetectionHistoryTimeWindowDays);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Location = new System.Drawing.Point(416, 8);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Name = "groupBoxDuplicateDetectionHistoryTimeWindow";
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Size = new System.Drawing.Size(384, 72);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.TabIndex = 6;
            this.groupBoxDuplicateDetectionHistoryTimeWindow.TabStop = false;
            this.groupBoxDuplicateDetectionHistoryTimeWindow.Text = "Duplicate Detection History Time Window";
            // 
            // lblDuplicateDetectionHistoryTimeWindowMilliseconds
            // 
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds.AutoSize = true;
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds.Location = new System.Drawing.Point(304, 24);
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds.Name = "lblDuplicateDetectionHistoryTimeWindowMilliseconds";
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds.Size = new System.Drawing.Size(67, 13);
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds.TabIndex = 15;
            this.lblDuplicateDetectionHistoryTimeWindowMilliseconds.Text = "Milliseconds:";
            // 
            // txtDuplicateDetectionHistoryTimeWindowMilliseconds
            // 
            this.txtDuplicateDetectionHistoryTimeWindowMilliseconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtDuplicateDetectionHistoryTimeWindowMilliseconds.Location = new System.Drawing.Point(304, 40);
            this.txtDuplicateDetectionHistoryTimeWindowMilliseconds.Name = "txtDuplicateDetectionHistoryTimeWindowMilliseconds";
            this.txtDuplicateDetectionHistoryTimeWindowMilliseconds.Size = new System.Drawing.Size(64, 20);
            this.txtDuplicateDetectionHistoryTimeWindowMilliseconds.TabIndex = 4;
            // 
            // lblDuplicateDetectionHistoryTimeWindowSeconds
            // 
            this.lblDuplicateDetectionHistoryTimeWindowSeconds.AutoSize = true;
            this.lblDuplicateDetectionHistoryTimeWindowSeconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDuplicateDetectionHistoryTimeWindowSeconds.Location = new System.Drawing.Point(232, 24);
            this.lblDuplicateDetectionHistoryTimeWindowSeconds.Name = "lblDuplicateDetectionHistoryTimeWindowSeconds";
            this.lblDuplicateDetectionHistoryTimeWindowSeconds.Size = new System.Drawing.Size(52, 13);
            this.lblDuplicateDetectionHistoryTimeWindowSeconds.TabIndex = 13;
            this.lblDuplicateDetectionHistoryTimeWindowSeconds.Text = "Seconds:";
            // 
            // txtDuplicateDetectionHistoryTimeWindowSeconds
            // 
            this.txtDuplicateDetectionHistoryTimeWindowSeconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtDuplicateDetectionHistoryTimeWindowSeconds.Location = new System.Drawing.Point(232, 40);
            this.txtDuplicateDetectionHistoryTimeWindowSeconds.Name = "txtDuplicateDetectionHistoryTimeWindowSeconds";
            this.txtDuplicateDetectionHistoryTimeWindowSeconds.Size = new System.Drawing.Size(64, 20);
            this.txtDuplicateDetectionHistoryTimeWindowSeconds.TabIndex = 3;
            // 
            // lblDuplicateDetectionHistoryTimeWindowMinutes
            // 
            this.lblDuplicateDetectionHistoryTimeWindowMinutes.AutoSize = true;
            this.lblDuplicateDetectionHistoryTimeWindowMinutes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDuplicateDetectionHistoryTimeWindowMinutes.Location = new System.Drawing.Point(160, 24);
            this.lblDuplicateDetectionHistoryTimeWindowMinutes.Name = "lblDuplicateDetectionHistoryTimeWindowMinutes";
            this.lblDuplicateDetectionHistoryTimeWindowMinutes.Size = new System.Drawing.Size(47, 13);
            this.lblDuplicateDetectionHistoryTimeWindowMinutes.TabIndex = 11;
            this.lblDuplicateDetectionHistoryTimeWindowMinutes.Text = "Minutes:";
            // 
            // txtDuplicateDetectionHistoryTimeWindowMinutes
            // 
            this.txtDuplicateDetectionHistoryTimeWindowMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.txtDuplicateDetectionHistoryTimeWindowMinutes.Location = new System.Drawing.Point(160, 40);
            this.txtDuplicateDetectionHistoryTimeWindowMinutes.Name = "txtDuplicateDetectionHistoryTimeWindowMinutes";
            this.txtDuplicateDetectionHistoryTimeWindowMinutes.Size = new System.Drawing.Size(64, 20);
            this.txtDuplicateDetectionHistoryTimeWindowMinutes.TabIndex = 2;
            // 
            // lblDuplicateDetectionHistoryTimeWindowHours
            // 
            this.lblDuplicateDetectionHistoryTimeWindowHours.AutoSize = true;
            this.lblDuplicateDetectionHistoryTimeWindowHours.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDuplicateDetectionHistoryTimeWindowHours.Location = new System.Drawing.Point(88, 24);
            this.lblDuplicateDetectionHistoryTimeWindowHours.Name = "lblDuplicateDetectionHistoryTimeWindowHours";
            this.lblDuplicateDetectionHistoryTimeWindowHours.Size = new System.Drawing.Size(38, 13);
            this.lblDuplicateDetectionHistoryTimeWindowHours.TabIndex = 9;
            this.lblDuplicateDetectionHistoryTimeWindowHours.Text = "Hours:";
            // 
            // lblDuplicateDetectionHistoryTimeWindowDays
            // 
            this.lblDuplicateDetectionHistoryTimeWindowDays.AutoSize = true;
            this.lblDuplicateDetectionHistoryTimeWindowDays.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDuplicateDetectionHistoryTimeWindowDays.Location = new System.Drawing.Point(16, 24);
            this.lblDuplicateDetectionHistoryTimeWindowDays.Name = "lblDuplicateDetectionHistoryTimeWindowDays";
            this.lblDuplicateDetectionHistoryTimeWindowDays.Size = new System.Drawing.Size(34, 13);
            this.lblDuplicateDetectionHistoryTimeWindowDays.TabIndex = 7;
            this.lblDuplicateDetectionHistoryTimeWindowDays.Text = "Days:";
            // 
            // txtDuplicateDetectionHistoryTimeWindowHours
            // 
            this.txtDuplicateDetectionHistoryTimeWindowHours.BackColor = System.Drawing.SystemColors.Window;
            this.txtDuplicateDetectionHistoryTimeWindowHours.Location = new System.Drawing.Point(88, 40);
            this.txtDuplicateDetectionHistoryTimeWindowHours.Name = "txtDuplicateDetectionHistoryTimeWindowHours";
            this.txtDuplicateDetectionHistoryTimeWindowHours.Size = new System.Drawing.Size(64, 20);
            this.txtDuplicateDetectionHistoryTimeWindowHours.TabIndex = 1;
            // 
            // txtDuplicateDetectionHistoryTimeWindowDays
            // 
            this.txtDuplicateDetectionHistoryTimeWindowDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtDuplicateDetectionHistoryTimeWindowDays.Location = new System.Drawing.Point(16, 40);
            this.txtDuplicateDetectionHistoryTimeWindowDays.Name = "txtDuplicateDetectionHistoryTimeWindowDays";
            this.txtDuplicateDetectionHistoryTimeWindowDays.Size = new System.Drawing.Size(64, 20);
            this.txtDuplicateDetectionHistoryTimeWindowDays.TabIndex = 0;
            // 
            // groupBoxFlags
            // 
            this.groupBoxFlags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFlags.Controls.Add(this.checkedListBox);
            this.groupBoxFlags.Location = new System.Drawing.Point(416, 184);
            this.groupBoxFlags.Name = "groupBoxFlags";
            this.groupBoxFlags.Size = new System.Drawing.Size(384, 112);
            this.groupBoxFlags.TabIndex = 7;
            this.groupBoxFlags.TabStop = false;
            this.groupBoxFlags.Text = "QueueDescription Settings";
            // 
            // checkedListBox
            // 
            this.checkedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox.CheckOnClick = true;
            this.checkedListBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkedListBox.FormattingEnabled = true;
            this.checkedListBox.Items.AddRange(new object[] {
            "Enable Batched Operations",
            "Enable Dead Lettering On Message Expiration",
            "Requires Duplicate Detection",
            "Requires Session"});
            this.checkedListBox.Location = new System.Drawing.Point(16, 28);
            this.checkedListBox.Margin = new System.Windows.Forms.Padding(8);
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(352, 64);
            this.checkedListBox.TabIndex = 0;
            this.checkedListBox.ThreeDCheckBoxes = true;
            // 
            // groupBoxLockDuration
            // 
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationMilliseconds);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationMilliseconds);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationSeconds);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationSeconds);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationMinutes);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationMinutes);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationHours);
            this.groupBoxLockDuration.Controls.Add(this.lblLockDurationDays);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationHours);
            this.groupBoxLockDuration.Controls.Add(this.txtLockDurationDays);
            this.groupBoxLockDuration.Location = new System.Drawing.Point(416, 96);
            this.groupBoxLockDuration.Name = "groupBoxLockDuration";
            this.groupBoxLockDuration.Size = new System.Drawing.Size(384, 72);
            this.groupBoxLockDuration.TabIndex = 8;
            this.groupBoxLockDuration.TabStop = false;
            this.groupBoxLockDuration.Text = "Lock Duration";
            // 
            // lblLockDurationMilliseconds
            // 
            this.lblLockDurationMilliseconds.AutoSize = true;
            this.lblLockDurationMilliseconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationMilliseconds.Location = new System.Drawing.Point(304, 24);
            this.lblLockDurationMilliseconds.Name = "lblLockDurationMilliseconds";
            this.lblLockDurationMilliseconds.Size = new System.Drawing.Size(67, 13);
            this.lblLockDurationMilliseconds.TabIndex = 15;
            this.lblLockDurationMilliseconds.Text = "Milliseconds:";
            // 
            // txtLockDurationMilliseconds
            // 
            this.txtLockDurationMilliseconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationMilliseconds.Location = new System.Drawing.Point(304, 40);
            this.txtLockDurationMilliseconds.Name = "txtLockDurationMilliseconds";
            this.txtLockDurationMilliseconds.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationMilliseconds.TabIndex = 4;
            // 
            // lblLockDurationSeconds
            // 
            this.lblLockDurationSeconds.AutoSize = true;
            this.lblLockDurationSeconds.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationSeconds.Location = new System.Drawing.Point(232, 24);
            this.lblLockDurationSeconds.Name = "lblLockDurationSeconds";
            this.lblLockDurationSeconds.Size = new System.Drawing.Size(52, 13);
            this.lblLockDurationSeconds.TabIndex = 13;
            this.lblLockDurationSeconds.Text = "Seconds:";
            // 
            // txtLockDurationSeconds
            // 
            this.txtLockDurationSeconds.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationSeconds.Location = new System.Drawing.Point(232, 40);
            this.txtLockDurationSeconds.Name = "txtLockDurationSeconds";
            this.txtLockDurationSeconds.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationSeconds.TabIndex = 3;
            // 
            // lblLockDurationMinutes
            // 
            this.lblLockDurationMinutes.AutoSize = true;
            this.lblLockDurationMinutes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationMinutes.Location = new System.Drawing.Point(160, 24);
            this.lblLockDurationMinutes.Name = "lblLockDurationMinutes";
            this.lblLockDurationMinutes.Size = new System.Drawing.Size(47, 13);
            this.lblLockDurationMinutes.TabIndex = 11;
            this.lblLockDurationMinutes.Text = "Minutes:";
            // 
            // txtLockDurationMinutes
            // 
            this.txtLockDurationMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationMinutes.Location = new System.Drawing.Point(160, 40);
            this.txtLockDurationMinutes.Name = "txtLockDurationMinutes";
            this.txtLockDurationMinutes.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationMinutes.TabIndex = 2;
            // 
            // lblLockDurationHours
            // 
            this.lblLockDurationHours.AutoSize = true;
            this.lblLockDurationHours.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationHours.Location = new System.Drawing.Point(88, 24);
            this.lblLockDurationHours.Name = "lblLockDurationHours";
            this.lblLockDurationHours.Size = new System.Drawing.Size(38, 13);
            this.lblLockDurationHours.TabIndex = 9;
            this.lblLockDurationHours.Text = "Hours:";
            // 
            // lblLockDurationDays
            // 
            this.lblLockDurationDays.AutoSize = true;
            this.lblLockDurationDays.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLockDurationDays.Location = new System.Drawing.Point(16, 24);
            this.lblLockDurationDays.Name = "lblLockDurationDays";
            this.lblLockDurationDays.Size = new System.Drawing.Size(34, 13);
            this.lblLockDurationDays.TabIndex = 7;
            this.lblLockDurationDays.Text = "Days:";
            // 
            // txtLockDurationHours
            // 
            this.txtLockDurationHours.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationHours.Location = new System.Drawing.Point(88, 40);
            this.txtLockDurationHours.Name = "txtLockDurationHours";
            this.txtLockDurationHours.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationHours.TabIndex = 1;
            // 
            // txtLockDurationDays
            // 
            this.txtLockDurationDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtLockDurationDays.Location = new System.Drawing.Point(16, 40);
            this.txtLockDurationDays.Name = "txtLockDurationDays";
            this.txtLockDurationDays.Size = new System.Drawing.Size(64, 20);
            this.txtLockDurationDays.TabIndex = 0;
            // 
            // groupBoxMaxQueueSizeInBytes
            // 
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.lblSizeInBytes);
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.txtSizeInBytes);
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.lblMessageCount);
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.lblMaxDeliveryCount);
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.txtMessageCount);
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.txtMaxDeliveryCount);
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.lblMaxQueueSizeInMegabytes);
            this.groupBoxMaxQueueSizeInBytes.Controls.Add(this.txtMaxQueueSizeInMegabytes);
            this.groupBoxMaxQueueSizeInBytes.Location = new System.Drawing.Point(16, 184);
            this.groupBoxMaxQueueSizeInBytes.Name = "groupBoxMaxQueueSizeInBytes";
            this.groupBoxMaxQueueSizeInBytes.Size = new System.Drawing.Size(384, 112);
            this.groupBoxMaxQueueSizeInBytes.TabIndex = 9;
            this.groupBoxMaxQueueSizeInBytes.TabStop = false;
            this.groupBoxMaxQueueSizeInBytes.Text = "Queue Properties:";
            // 
            // lblSizeInBytes
            // 
            this.lblSizeInBytes.AutoSize = true;
            this.lblSizeInBytes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSizeInBytes.Location = new System.Drawing.Point(200, 64);
            this.lblSizeInBytes.Name = "lblSizeInBytes";
            this.lblSizeInBytes.Size = new System.Drawing.Size(71, 13);
            this.lblSizeInBytes.TabIndex = 22;
            this.lblSizeInBytes.Text = "Size In Bytes:";
            // 
            // txtSizeInBytes
            // 
            this.txtSizeInBytes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSizeInBytes.BackColor = System.Drawing.SystemColors.Window;
            this.txtSizeInBytes.Location = new System.Drawing.Point(200, 80);
            this.txtSizeInBytes.Name = "txtSizeInBytes";
            this.txtSizeInBytes.ReadOnly = true;
            this.txtSizeInBytes.Size = new System.Drawing.Size(168, 20);
            this.txtSizeInBytes.TabIndex = 21;
            // 
            // lblMessageCount
            // 
            this.lblMessageCount.AutoSize = true;
            this.lblMessageCount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblMessageCount.Location = new System.Drawing.Point(16, 64);
            this.lblMessageCount.Name = "lblMessageCount";
            this.lblMessageCount.Size = new System.Drawing.Size(84, 13);
            this.lblMessageCount.TabIndex = 20;
            this.lblMessageCount.Text = "Message Count:";
            // 
            // lblMaxDeliveryCount
            // 
            this.lblMaxDeliveryCount.AutoSize = true;
            this.lblMaxDeliveryCount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblMaxDeliveryCount.Location = new System.Drawing.Point(16, 24);
            this.lblMaxDeliveryCount.Name = "lblMaxDeliveryCount";
            this.lblMaxDeliveryCount.Size = new System.Drawing.Size(102, 13);
            this.lblMaxDeliveryCount.TabIndex = 18;
            this.lblMaxDeliveryCount.Text = "Max Delivery Count:";
            // 
            // txtMessageCount
            // 
            this.txtMessageCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMessageCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMessageCount.Location = new System.Drawing.Point(16, 80);
            this.txtMessageCount.Name = "txtMessageCount";
            this.txtMessageCount.ReadOnly = true;
            this.txtMessageCount.Size = new System.Drawing.Size(168, 20);
            this.txtMessageCount.TabIndex = 19;
            // 
            // txtMaxDeliveryCount
            // 
            this.txtMaxDeliveryCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxDeliveryCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMaxDeliveryCount.Location = new System.Drawing.Point(16, 40);
            this.txtMaxDeliveryCount.Name = "txtMaxDeliveryCount";
            this.txtMaxDeliveryCount.Size = new System.Drawing.Size(168, 20);
            this.txtMaxDeliveryCount.TabIndex = 17;
            // 
            // lblMaxQueueSizeInMegabytes
            // 
            this.lblMaxQueueSizeInMegabytes.AutoSize = true;
            this.lblMaxQueueSizeInMegabytes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblMaxQueueSizeInMegabytes.Location = new System.Drawing.Point(200, 24);
            this.lblMaxQueueSizeInMegabytes.Name = "lblMaxQueueSizeInMegabytes";
            this.lblMaxQueueSizeInMegabytes.Size = new System.Drawing.Size(120, 13);
            this.lblMaxQueueSizeInMegabytes.TabIndex = 16;
            this.lblMaxQueueSizeInMegabytes.Text = "Max Size In Megabytes:";
            // 
            // txtMaxQueueSizeInMegabytes
            // 
            this.txtMaxQueueSizeInMegabytes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxQueueSizeInMegabytes.BackColor = System.Drawing.SystemColors.Window;
            this.txtMaxQueueSizeInMegabytes.Location = new System.Drawing.Point(200, 40);
            this.txtMaxQueueSizeInMegabytes.Name = "txtMaxQueueSizeInMegabytes";
            this.txtMaxQueueSizeInMegabytes.Size = new System.Drawing.Size(168, 20);
            this.txtMaxQueueSizeInMegabytes.TabIndex = 0;
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnAction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAction.Location = new System.Drawing.Point(648, 304);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(72, 24);
            this.btnAction.TabIndex = 0;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = false;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Location = new System.Drawing.Point(728, 304);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 24);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // HandleQueueControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.groupBoxMaxQueueSizeInBytes);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.groupBoxLockDuration);
            this.Controls.Add(this.groupBoxFlags);
            this.Controls.Add(this.groupBoxDuplicateDetectionHistoryTimeWindow);
            this.Controls.Add(this.groupBoxDefaultMessageTimeToLive);
            this.Controls.Add(this.groupBoxPath);
            this.Name = "HandleQueueControl";
            this.Size = new System.Drawing.Size(816, 344);
            this.groupBoxPath.ResumeLayout(false);
            this.groupBoxPath.PerformLayout();
            this.groupBoxDefaultMessageTimeToLive.ResumeLayout(false);
            this.groupBoxDefaultMessageTimeToLive.PerformLayout();
            this.groupBoxDuplicateDetectionHistoryTimeWindow.ResumeLayout(false);
            this.groupBoxDuplicateDetectionHistoryTimeWindow.PerformLayout();
            this.groupBoxFlags.ResumeLayout(false);
            this.groupBoxLockDuration.ResumeLayout(false);
            this.groupBoxLockDuration.PerformLayout();
            this.groupBoxMaxQueueSizeInBytes.ResumeLayout(false);
            this.groupBoxMaxQueueSizeInBytes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxPath;
        private System.Windows.Forms.GroupBox groupBoxDefaultMessageTimeToLive;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveMilliseconds;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveMilliseconds;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveSeconds;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveSeconds;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveMinutes;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveMinutes;
        private System.Windows.Forms.Label lbllblDefaultMessageTimeToLiveHours;
        private System.Windows.Forms.Label lblDefaultMessageTimeToLiveDays;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveHours;
        private System.Windows.Forms.TextBox txtDefaultMessageTimeToLiveDays;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.GroupBox groupBoxDuplicateDetectionHistoryTimeWindow;
        private System.Windows.Forms.Label lblDuplicateDetectionHistoryTimeWindowMilliseconds;
        private System.Windows.Forms.TextBox txtDuplicateDetectionHistoryTimeWindowMilliseconds;
        private System.Windows.Forms.Label lblDuplicateDetectionHistoryTimeWindowSeconds;
        private System.Windows.Forms.TextBox txtDuplicateDetectionHistoryTimeWindowSeconds;
        private System.Windows.Forms.Label lblDuplicateDetectionHistoryTimeWindowMinutes;
        private System.Windows.Forms.TextBox txtDuplicateDetectionHistoryTimeWindowMinutes;
        private System.Windows.Forms.Label lblDuplicateDetectionHistoryTimeWindowHours;
        private System.Windows.Forms.Label lblDuplicateDetectionHistoryTimeWindowDays;
        private System.Windows.Forms.TextBox txtDuplicateDetectionHistoryTimeWindowHours;
        private System.Windows.Forms.TextBox txtDuplicateDetectionHistoryTimeWindowDays;
        private System.Windows.Forms.GroupBox groupBoxFlags;
        private System.Windows.Forms.CheckedListBox checkedListBox;
        private System.Windows.Forms.GroupBox groupBoxLockDuration;
        private System.Windows.Forms.Label lblLockDurationMilliseconds;
        private System.Windows.Forms.TextBox txtLockDurationMilliseconds;
        private System.Windows.Forms.Label lblLockDurationSeconds;
        private System.Windows.Forms.TextBox txtLockDurationSeconds;
        private System.Windows.Forms.Label lblLockDurationMinutes;
        private System.Windows.Forms.TextBox txtLockDurationMinutes;
        private System.Windows.Forms.Label lblLockDurationHours;
        private System.Windows.Forms.Label lblLockDurationDays;
        private System.Windows.Forms.TextBox txtLockDurationHours;
        private System.Windows.Forms.TextBox txtLockDurationDays;
        private System.Windows.Forms.GroupBox groupBoxMaxQueueSizeInBytes;
        private System.Windows.Forms.TextBox txtMaxQueueSizeInMegabytes;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label lblMessageCount;
        private System.Windows.Forms.Label lblMaxDeliveryCount;
        private System.Windows.Forms.TextBox txtMessageCount;
        private System.Windows.Forms.TextBox txtMaxDeliveryCount;
        private System.Windows.Forms.Label lblMaxQueueSizeInMegabytes;
        private System.Windows.Forms.Label lblSizeInBytes;
        private System.Windows.Forms.TextBox txtSizeInBytes;
    }
}
