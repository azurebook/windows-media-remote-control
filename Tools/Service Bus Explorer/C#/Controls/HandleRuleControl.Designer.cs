﻿namespace Microsoft.AppFabric.CAT.WindowsAzure.Samples.ServiceBusExplorer
{
    partial class HandleRuleControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxName = new System.Windows.Forms.GroupBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnAction = new System.Windows.Forms.Button();
            this.groupBoxSqlFilterExpression = new System.Windows.Forms.GroupBox();
            this.txtSqlFilterExpression = new System.Windows.Forms.TextBox();
            this.groupBoxSqlFilterAction = new System.Windows.Forms.GroupBox();
            this.txtSqlFilterAction = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBoxDefault = new System.Windows.Forms.GroupBox();
            this.checkBoxDefault = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxName.SuspendLayout();
            this.groupBoxSqlFilterExpression.SuspendLayout();
            this.groupBoxSqlFilterAction.SuspendLayout();
            this.groupBoxDefault.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxName
            // 
            this.groupBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxName.Controls.Add(this.txtName);
            this.groupBoxName.Location = new System.Drawing.Point(16, 8);
            this.groupBoxName.Name = "groupBoxName";
            this.groupBoxName.Size = new System.Drawing.Size(664, 88);
            this.groupBoxName.TabIndex = 4;
            this.groupBoxName.TabStop = false;
            this.groupBoxName.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(16, 40);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(632, 20);
            this.txtName.TabIndex = 0;
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnAction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAction.Location = new System.Drawing.Point(648, 304);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(72, 24);
            this.btnAction.TabIndex = 0;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = false;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // groupBoxSqlFilterExpression
            // 
            this.groupBoxSqlFilterExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSqlFilterExpression.Controls.Add(this.txtSqlFilterExpression);
            this.groupBoxSqlFilterExpression.Location = new System.Drawing.Point(16, 104);
            this.groupBoxSqlFilterExpression.Name = "groupBoxSqlFilterExpression";
            this.groupBoxSqlFilterExpression.Size = new System.Drawing.Size(384, 88);
            this.groupBoxSqlFilterExpression.TabIndex = 12;
            this.groupBoxSqlFilterExpression.TabStop = false;
            this.groupBoxSqlFilterExpression.Text = "Filter";
            // 
            // txtSqlFilterExpression
            // 
            this.txtSqlFilterExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlFilterExpression.BackColor = System.Drawing.SystemColors.Window;
            this.txtSqlFilterExpression.Location = new System.Drawing.Point(16, 40);
            this.txtSqlFilterExpression.Name = "txtSqlFilterExpression";
            this.txtSqlFilterExpression.Size = new System.Drawing.Size(352, 20);
            this.txtSqlFilterExpression.TabIndex = 0;
            // 
            // groupBoxSqlFilterAction
            // 
            this.groupBoxSqlFilterAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSqlFilterAction.Controls.Add(this.txtSqlFilterAction);
            this.groupBoxSqlFilterAction.Location = new System.Drawing.Point(416, 104);
            this.groupBoxSqlFilterAction.Name = "groupBoxSqlFilterAction";
            this.groupBoxSqlFilterAction.Size = new System.Drawing.Size(384, 88);
            this.groupBoxSqlFilterAction.TabIndex = 13;
            this.groupBoxSqlFilterAction.TabStop = false;
            this.groupBoxSqlFilterAction.Text = "Action";
            // 
            // txtSqlFilterAction
            // 
            this.txtSqlFilterAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlFilterAction.BackColor = System.Drawing.SystemColors.Window;
            this.txtSqlFilterAction.Location = new System.Drawing.Point(16, 40);
            this.txtSqlFilterAction.Name = "txtSqlFilterAction";
            this.txtSqlFilterAction.Size = new System.Drawing.Size(352, 20);
            this.txtSqlFilterAction.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Location = new System.Drawing.Point(728, 304);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 24);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBoxDefault
            // 
            this.groupBoxDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxDefault.Controls.Add(this.checkBoxDefault);
            this.groupBoxDefault.Location = new System.Drawing.Point(696, 8);
            this.groupBoxDefault.Name = "groupBoxDefault";
            this.groupBoxDefault.Size = new System.Drawing.Size(104, 88);
            this.groupBoxDefault.TabIndex = 19;
            this.groupBoxDefault.TabStop = false;
            this.groupBoxDefault.Text = "Is Default Rule?";
            // 
            // checkBoxDefault
            // 
            this.checkBoxDefault.AutoSize = true;
            this.checkBoxDefault.Location = new System.Drawing.Point(16, 24);
            this.checkBoxDefault.Name = "checkBoxDefault";
            this.checkBoxDefault.Size = new System.Drawing.Size(60, 17);
            this.checkBoxDefault.TabIndex = 0;
            this.checkBoxDefault.Text = "Default";
            this.checkBoxDefault.UseVisualStyleBackColor = true;
            this.checkBoxDefault.CheckedChanged += new System.EventHandler(this.checkBoxDefault_CheckedChanged);
            // 
            // HandleRuleControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.groupBoxDefault);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBoxSqlFilterAction);
            this.Controls.Add(this.groupBoxSqlFilterExpression);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.groupBoxName);
            this.Name = "HandleRuleControl";
            this.Size = new System.Drawing.Size(816, 344);
            this.groupBoxName.ResumeLayout(false);
            this.groupBoxName.PerformLayout();
            this.groupBoxSqlFilterExpression.ResumeLayout(false);
            this.groupBoxSqlFilterExpression.PerformLayout();
            this.groupBoxSqlFilterAction.ResumeLayout(false);
            this.groupBoxSqlFilterAction.PerformLayout();
            this.groupBoxDefault.ResumeLayout(false);
            this.groupBoxDefault.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.GroupBox groupBoxSqlFilterExpression;
        private System.Windows.Forms.TextBox txtSqlFilterExpression;
        private System.Windows.Forms.GroupBox groupBoxSqlFilterAction;
        private System.Windows.Forms.TextBox txtSqlFilterAction;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBoxDefault;
        private System.Windows.Forms.CheckBox checkBoxDefault;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
